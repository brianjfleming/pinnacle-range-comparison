# Pinnacle Range Comparison Plugin #

WordPress plugin built for pinnacleaviation.com to utilize aircraft database and output a JavaScript and Google 
Maps API application to compare ranges of all aircraft in the database. Registers "Map Cities" custom post type
to manage which cities are included on the range comparison map.

## File Structure

```
/cpt
    All files for registering the "Map Cities" custom post type 
    and management of that post type.
/data
    Holds default core data to insert at time of database installation.
/db 
    All database support, including installation, updates, and 
    model methods for accessing data.
/display 
    All output functionality, including widgets and shortcodes
/dist 
    Minified JavasScript and CSS files for enqueue. Do not edit.
/src 
    Source JavasScript and SASS files
```

### Webpack Setup

Ensure webpack and webpack-cli are installed globally:
```
$ npm install -g webpack 
$ npm install -g webpack-cli
```

To install dependencies:
```
$ npm install
```

To watch SRC files for changes:
```
$ npm start
```

To Create minified files for distribution:
```
$ npm run build
```

### Changelog

#### v0.0.1 - Base Plugin

* README formatting.
* Plugin File.
* Initial Commits.