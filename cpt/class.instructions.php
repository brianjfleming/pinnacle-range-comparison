<?php

namespace PinnacleAviation\Plugins\RangeComparison\CPT;

class Instructions {

    public static function register_instructions_page() {
        add_submenu_page(
            'edit.php?post_type=prc_map_cities',
            __( 'How To Use' ),
            __( 'How To Use' ),
            'manage_options',
            'range_comparison_instructions',
            array( __NAMESPACE__ . '\\Instructions', 'plugin_instructions_output' )
        );

        function mt_settings_page() {
            echo "<h2>" . __( 'Test Settings', 'menu-test' ) . "</h2>";
        }
    }

    public static function plugin_instructions_output() {

        ?>

        <div class="wrap">
            <h1 class="wp-heading-inline">How to Use Range Comparison Map</h1>
            <hr class="wp-header-end" style="margin: 0 0 40px 0;">

            <div class="postbox"><div class="inside">
                    <h3>Charter Fleets Integration</h3>
                    <p>
                        The <strong>Charter Fleets</strong> custom post type recieves a new post meta box at the bottom
                        of each individual post edit page, allowing administrators to link a Charter fleet plane with a
                        specific plane model in the database.
                    </p>
                    <p>
                        This works in conjuction with the Range Comparison Map, providing a list of Limiited planes when
                        the map group is set to <strong>charter</strong>.
                    </p>
                </div></div>

            <div class="postbox"><div class="inside">
                <h3>Map Cities Custom Post Type</h3>
                <p>
                    The <strong>Map Cities</strong> custom post type is available to customize which cities show on the
                    <strong>Range Comparison Map</strong>. This allows you to add, remove, and edit any available city
                    which can be used as a center-point for range comparison.
                </p>
                <p>
                    Each city supports a <strong>Name</strong>, <strong>Latitude/Longitude</strong>, and can be set to
                    represent a <strong>Pinnacle Aviation Headquarters</strong>.
                </p>
                <p>
                    All cities added as a <strong>Map City</strong> are automatically represented on the
                    <strong>Range Comparison Map</strong>.
                </p>
            </div></div>

            <div class="postbox"><div class="inside">
                <h3>Widgets and Shortcodes</h3>
                <p>
                    There are <strong>two</strong> primary display components which are <strong>both required</strong>
                    to implement a working <strong>Range Comparison Map</strong> on your website. The <strong>Map</strong>
                    itself, as well as a seperate <strong>Map Options</strong> component. Both components can be placed on
                    the page using either a <strong>WordPress Widget</strong> OR a <strong>WordPress Shortcode</strong>
                </p>

                <h4>Outputing the Map Component</h4>
                <p>
                    The map component can be output using the <strong>Range Comparison Map</strong> Widget or Shortcode.
                    Both the Widget AND the Shortcode have an available option to declare the rendered height of the map
                    component, as well as limit the group of displayable aircraft. Additionally, they serve to declare
                    placement on the page. The shortcode can be input like so:
                    <code>[range_comparison_map height='{750px}' group='{charter|all}']</code>
                </p>
                <p>
                    The default height, if none is provided is <strong>700px</strong>
                </p>
                <p>
                    The default group, if none is provided is <strong>all</strong>. Option can be set to "all" or
                    "charter" to limit the aircraft which are selectable to ONLY planes in the 'charter-fleets' section
                    which have been linked to a Jet or Turboprop in the database.
                </p>

                <h4>Outputting the Map Options Component</h4>
                <p>
                    The map options component can be output using the <strong>Range Comparison Options</strong> Widget
                    or Shortcode. Both the Widget AND the Shortcode have an available option to declare the label text
                    to be used for the type of range you want to output on the map. Additionally, they serve to declare
                    placement on the page. The shortcode can be implemented like so:
                    <code>[range_comparison_options label='My Label']</code>
                </p>
                <p>
                    The default label text is <strong>Select Range to Show:</strong>
                </p>
            </div></div>

        </div>

        <?php

    }


}