<?php

namespace PinnacleAviation\Plugins\RangeComparison\CPT;

class Labor_Rates {

	public static function register_labor_rates_page() {
		add_submenu_page(
			'edit.php?post_type=charter-fleets',
			__( 'Labor Rates' ),
			__( 'Labor Rates' ),
			'manage_options',
			'range_comparison_labor_rates',
			array( __NAMESPACE__ . '\\Labor_Rates', 'plugin_labor_rates_output' )
		);
	}

	private static function get_labor_rates_options() {

		return array(
			'lr_fuel_cost' => 'Fuel Cost',
			'lr_jets_cost' => 'Labor Rate (Jets)',
			'lr_prop_cost' => 'Labor Rate (Props)',
		);

	}

	public static function get_labor_rates_values() {

		$values = array();

		foreach( self::get_labor_rates_options() as $option => $label ) {
			$values[$option] = get_option($option);
		}

		return $values;

	}

	public static function plugin_labor_rates_output() {

		if(
			isset( $_POST["lr_settings_nonce"] ) &&
			wp_verify_nonce( $_POST["lr_settings_nonce"], "lr_settings" )
		) {
			self::save_labor_rates();
		}

		?>
		<div class="wrap">
            <h1 class="wp-heading-inline">Labor Rates</h1>

            <form method="post" id="lr_settings" name="lr_settings">

	            <?php wp_nonce_field( "lr_settings", "lr_settings_nonce", true, true ); ?>
	            <table class="form-table" role="presentation">
		            <tbody>

		            <?php foreach( self::get_labor_rates_options() as $option => $label ) { ?>

			                <tr>
				                <th scope="row">
					                <label for="<?php echo $option; ?>"><?php echo $label; ?></label>
				                </th>
				                <td>
					                <input
						                name="<?php echo $option; ?>"
						                id="<?php echo $option; ?>"
						                type="text"
						                class="regular-text ltr"
						                value="<?php echo get_option( $option ); ?>">
				                </td>
			                </tr>

	                <?php } ?>

		            </tbody>
	            </table>

	            <p class="submit">
		            <input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes">
	            </p>
            </form>

		</div>
		<?php

	}

	public static function save_labor_rates() {

		foreach ( self::get_labor_rates_options() as $option => $label ) {
			update_option( $option, $_POST[$option] );
		}

		// Admin Notice.
		echo "<div class=\"notice notice-success is-dismissible\">";
		echo "<p>Labor Rates Settings Saved.</p>";
		echo "</div>";

	}

}