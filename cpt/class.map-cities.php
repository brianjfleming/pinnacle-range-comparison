<?php

namespace PinnacleAviation\Plugins\RangeComparison\CPT;

class Map_Cities {

    /**
     * Register our Map Cities Custom Post Type.
     */
    public static function register_post_type() {

        // Register the Post Type.
        register_post_type(
            MAP_CITIES_POST_TYPE,
            array(
                'label' => __( 'Map Cities' ),
                'description' => __( 'Cities which should appear on the Range Comparison Map.' ),
                'menu_icon' => 'dashicons-admin-site',
                'public' => true,
                'show_ui' => true,
                'show_in_menu' => true,
                'menu_position' => 20,
                'capability_type' => 'post',
                'hierarchical' => false,
                'has_archive' => false,
                'supports' => array( 'title' ),
                'labels' => self::labels(),
                'capabilities' => self::capabilities(),
                'register_meta_box_cb' => array( __NAMESPACE__ . '\\Map_Cities', 'post_meta_box' )
            )
        );

        // Add Save Hook for Post Meta data.
        add_action( 'save_post_' . MAP_CITIES_POST_TYPE, array( __NAMESPACE__ . '\\Map_Cities', 'save_post_meta' ) );

        // Add custom columns.
        add_filter('manage_' . MAP_CITIES_POST_TYPE . '_posts_columns', array( __NAMESPACE__ . '\\Map_Cities', 'filter_cpt_columns' ) );

        // Custom Columns Content.
        add_action( 'manage_' . MAP_CITIES_POST_TYPE . '_posts_custom_column', array( __NAMESPACE__ . '\\Map_Cities', 'cpt_columns_content' ), 10, 2);

        // Bulk Edit Support
        add_action( MAP_CITIES_POST_TYPE . '_bulk_edit',  array( __NAMESPACE__ . '\\Map_Cities', 'bulk_edit_fields' ), 10, 2);

    }

    /**
     * Labels array for prc_map_cities post type.
     *
     * @return array
     */
    public static function labels() {

        return array(
            'name' => __( 'Map Cities' ),
            'singular_name' => __( 'Map City' ),
            'menu_name' => __( 'Map Cities' ),
            'add_new' => __( 'Add Map City' ),
            'add_new_item' => __( 'Add New Map City' ),
            'edit' => __( 'Edit Map City' ),
            'edit_item' => __( 'Edit Map City' ),
            'view' => __( 'View Map City' ),
            'view_item' => __( 'View Map City' ),
            'search_items' => __( 'Search Map Cities' ),
            'not_found' => __( 'No Map Cities Found' ),
            'not_found_in_trash' => __( 'No Map Cities Found In Trash' ),
            'parent' => __( 'Parent Map City' )
        );

    }

    /**
     * Capabilities array for prc_map_cities post type.
     *
     * @return array
     */
    public static function capabilities() {

        return array(
            'edit_post'          => RANGE_COMPARISON_CAPABILITY,
            'edit_posts'         => RANGE_COMPARISON_CAPABILITY,
            'edit_others_posts'  => RANGE_COMPARISON_CAPABILITY,
            'publish_posts'      => RANGE_COMPARISON_CAPABILITY,
            'read_post'          => RANGE_COMPARISON_CAPABILITY,
            'read_private_posts' => RANGE_COMPARISON_CAPABILITY,
            'delete_post'        => RANGE_COMPARISON_CAPABILITY,
            'delete_posts'       => RANGE_COMPARISON_CAPABILITY
        );

    }

    /**
     * Register the MetaBox
     */
    public static function post_meta_box() {

        add_meta_box(
            'map-city-details',
            __( 'Details' ),
            array( __NAMESPACE__ . '\\Map_Cities', 'post_meta_box_content' ),
            MAP_CITIES_POST_TYPE
        );

    }

    /**
     * Populate the content of our metabox.
     *
     * @param Object $post  The post object
     */
    public static function post_meta_box_content( $post ) {

        // Add a nonce field so we can check for it later.
        wp_nonce_field( 'map_cities_nonce', 'map_cities_nonce' );

        // Get Post Meta Values.
        $lat = get_post_meta( $post->ID, '_lat', true );
        $lng = get_post_meta( $post->ID, '_lng', true );
        $continent = get_post_meta( $post->ID, '_continent', true );
        $hq = get_post_meta( $post->ID, '_hq', true );

        // Define Continents.
        $continent_choices = array(
            'north_america' => 'North America',
            'africa' => 'Africa',
            'asia' => 'Asia',
            'australia' => 'Australia',
            'europe' => 'Europe',
            'south_america' => 'South America'
        );

        echo '<p>';
        echo '<label for="_lat">Latitude</label>';
        echo '<input type="text" style="width:100%" id="_lat" name="_lat" value="' . esc_attr( $lat ) . '" />';
        echo '</p>';

        echo '<p>';
        echo '<label for="_lng">Longitude</label>';
        echo '<input type="text" style="width:100%" id="_lng" name="_lng" value="' . esc_attr( $lng ) . '" />';
        echo '</p>';

        echo '<p>';
        echo '<label for="_continent">Continent</label>';
        echo '<select style="width:100%" id="_continent" name="_continent">';

        foreach( $continent_choices as $value => $label ) {

            $selected = $value === $continent ? ' selected' : '';
            echo "<option value='{$value}'{$selected}>{$label}</option>";

        }

        echo '</select>';
        echo '</p>';

        echo '<p>';
        echo '<label for="_hq">HQ City?</label>';
        echo '<select style="width:100%" id="_hq" name="_hq">';

        echo '<option value="0"';
        echo '0' === $hq ? ' selected>' : '>';
        echo 'No';
        echo '</option>';

        echo '<option value="1"';
        echo '1' === $hq ? ' selected>' : '>';
        echo 'Yes';
        echo '</option>';

        echo '</select>';
        echo '</p>';

    }

    /**
     * Save our custom post meta.
     *
     * @param int $post_id  The post ID.
     */
    public static function save_post_meta( $post_id ) {

        // Check if our nonce is set.
        if ( ! isset( $_POST['map_cities_nonce'] ) ) {
            return;
        }

        // Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $_POST['map_cities_nonce'], 'map_cities_nonce' ) ) {
            return;
        }

        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }

        // Check the user's permissions and our post type.
        if ( ! current_user_can( RANGE_COMPARISON_CAPABILITY ) ) {
            return;
        }

        /* OK, it's safe for us to save the data now. */

        // Sanitize user input and update post meta.
        $lat = sanitize_text_field( $_POST['_lat'] );
        update_post_meta( $post_id, '_lat', $lat );

        $lng = sanitize_text_field( $_POST['_lng'] );
        update_post_meta( $post_id, '_lng', $lng );

        $continent = sanitize_text_field( $_POST['_continent'] );
        update_post_meta( $post_id, '_continent', $continent );

        $hq = sanitize_text_field( $_POST['_hq'] );
        update_post_meta( $post_id, '_hq', $hq );


    }

    public static function filter_cpt_columns( $columns ) {

        $columns['lat']       = __( 'Latitude' );
        $columns['lng']       = __( 'Longitude' );
        $columns['continent'] = __( 'Continent' );
        $columns['hq']        = __( 'Pinnacle Headquarters' );

        return $columns;

    }

    public static function cpt_columns_content( $column, $post_id ) {

        switch( $column ) {
            case "lat":
                echo get_post_meta( $post_id, '_lat', true );
                break;
            case "lng":
                echo get_post_meta( $post_id, '_lng',true );
                break;
            case "continent":
                echo ucwords( str_replace( '_', ' ', get_post_meta( $post_id, '_continent', true ) ) );
                break;
            case "hq":
                echo '1' === get_post_meta( $post_id, '_hq', true ) ? 'Yes' : 'No';
                break;
        }

    }

    public static function bulk_edit_fields( $column_name, $post_type ) {

        switch( $column_name ) :
            case 'price': {

                // you can also print Nonce here, do not do it ouside the switch() because it will be printed many times
                wp_nonce_field( 'misha_q_edit_nonce', 'misha_nonce' );

                // please note: the <fieldset> classes could be:
                // inline-edit-col-left, inline-edit-col-center, inline-edit-col-right
                // each class for each column, all columns are float:left,
                // so, if you want a left column, use clear:both element before
                // the best way to use classes here is to look in browser "inspect element" at the other fields

                // for the FIRST column only, it opens <fieldset> element, all our fields will be there
                echo '<fieldset class="inline-edit-col-right">
				<div class="inline-edit-col">
					<div class="inline-edit-group wp-clearfix">';

                echo '<label class="alignleft">
					<span class="title">Price</span>
					<span class="input-text-wrap"><input type="text" name="price" value=""></span>
				</label>';

                break;

            }
            case 'featured': {

                echo '<label class="alignleft">
					<input type="checkbox" name="featured">
					<span class="checkbox-title">Featured product</span>
				</label>';

                // for the LAST column only - closing the fieldset element
                echo '</div></div></fieldset>';

                break;

            }

        endswitch;

    }

}