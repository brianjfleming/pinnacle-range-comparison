<?php

namespace PinnacleAviation\Plugins\RangeComparison\CPT;

class Rate_Sheet {

	public static function register_rate_sheet_page() {
		add_submenu_page(
			'edit.php?post_type=charter-fleets',
			__( 'Rate Sheet' ),
			__( 'Rate Sheet' ),
			'manage_options',
			'range_comparison_rate_sheet',
			array( __NAMESPACE__ . '\\Rate_Sheet', 'plugin_rate_sheet_output' )
		);
	}

	public static function plugin_rate_sheet_output() {

		if(
			isset( $_POST["rs_settings_nonce"] ) &&
			wp_verify_nonce( $_POST["rs_settings_nonce"], "rs_settings" )
		) {
			self::save_rate_sheet();
		}

		?>
		<div class="wrap">
            <h1 class="wp-heading-inline">Rate Sheet</h1>

            <?php
            $current_rate_sheet_url = get_option( 'pa_rate_sheet_url', false );

            if( $current_rate_sheet_url ) {
                echo "Current Rate Sheet: <a href='{$current_rate_sheet_url}' target='_blank'>Rate Sheet</a>";
            } else {
                echo "No Rate Sheet Currently Uploaded";
            }
            ?>

            <form method="post" id="rs_settings" name="rs_settings" enctype="multipart/form-data">

	            <?php wp_nonce_field( "rs_settings", "rs_settings_nonce", true, true ); ?>
	            <table class="form-table" role="presentation">
		            <tbody>
                        <tr>
                            <th scope="row">
                                <label for="rate_sheet">Upload New Rate Sheet</label>
                            </th>
                            <td>
                                <input
                                    type="file"
                                    name="rate_sheet"
                            </td>
                        </tr>
		            </tbody>
	            </table>

	            <p class="submit">
		            <input type="submit" name="submit" id="submit" class="button button-primary" value="Upload">
	            </p>
            </form>

		</div>
		<?php

	}

	public static function save_rate_sheet() {

	    if( '' === $_FILES['rate_sheet']['name'] ) {
	        return;
        }

	    $uploaded_file = $_FILES['rate_sheet'];

	    $move_file = wp_handle_upload( $uploaded_file, false );

	    // If we had errors, error, and bail.
	    if( ! $move_file || isset( $move_file['error'] ) ) {
		    echo "<div class=\"notice notice-error is-dismissible\">";

		    if( ! $move_file ) {
			    echo "<p>File Upload failed. Please contact your administrator.</p>";
		    } else {
			    echo "<p>";
			    echo $move_file['error'];
			    echo "</p>";
		    }
		    echo "</div>";

		    return;
        }

	    // Update our option with our new file.
	    update_option( 'pa_rate_sheet_url', $move_file['url'] );

		// Admin Notice of success.
		echo "<div class=\"notice notice-success is-dismissible\">";
		echo "<p>Rate Sheet Successfully Uploaded.</p>";
		echo "</div>";

	}

}