<?php

namespace PinnacleAviation\Plugins\RangeComparison\DB;

/**
 * Class DB_Init
 * @package PinnacleAviation\Plugins\RangeComparison
 */
class DB_Init {

    /**
     * Checks to see if our dB schema/tables needs to be installed or updated. If installation is necessary, we run
     * our installer. If updating is necessary, we run through our dbDelta updates, until we reach the newest version.
     * Installed version is saved as a site option or network option (if running multisite) under the key defined as
     * DB_VERSION_OPTION_KEY.
     *
     * @return int  The version we have installed or updated to, as an integer.
     */
    public static function install_update_check() {

        $installed_db_version = get_site_option( DB_VERSION_OPTION_KEY, false );

        // If the option does not exist, go through initial installation.
        if( ! $installed_db_version ) {

            require_once dirname( __FILE__ ) . '/class.db-install.php';
            $installed_db_version = DB_Install::run();

        }

        // Check to see if we are up to date or not.
        if( intval( $installed_db_version ) < intval( RANGE_COMPARISON_DB_VERSION ) ) {

            require_once dirname( __FILE__ ) . '/class.db-update.php';
            $installed_db_version = DB_Update::run();

        }

        return $installed_db_version;

    }

}