<?php


namespace PinnacleAviation\Plugins\RangeComparison\DB;

/**
 * Class DB_Install
 * @package PinnacleAviation\Plugins\RangeComparison
 */
class DB_Install {

    /**
     * Execute our Installation
     *
     * @return int
     */
    public static function run() {

        // We need access to WP's dbDelta() function
        require_once ABSPATH . 'wp-admin/includes/upgrade.php';

        // Create dependency tables.
        $planes_master_debug      = self::create_planes_2021_table();
        $planes_master_prop_debug = self::create_planes_prop_2021_table();

        // Update our dB version, and return our version number.
        add_site_option( DB_VERSION_OPTION_KEY, 1 );
        return 1;

    }

    /**
     * Required by Range Comparison.
     *
     * @return array
     */
    private static function create_planes_2021_table() {

        global $wpdb;

        $prefix          = $wpdb->base_prefix;
        $table_name      = $prefix . 'planes_2021';
        $charset_collate = $wpdb->get_charset_collate();

        $planes_2021_sql = <<<SQL
CREATE TABLE `$table_name` (
  `ID` int(24) NOT NULL AUTO_INCREMENT,
  `AcMgfName` varchar(255) NOT NULL,
  `AcName` varchar(255) NOT NULL,
  `GroupName` varchar(255) NOT NULL,
  `FuelFlow` varchar(255) NOT NULL,
  `FuelCostGal` varchar(255) NOT NULL,
  `MaintLabor` varchar(255) NOT NULL,
  `LaborCost` varchar(255) NOT NULL,
  `Parts` varchar(255) NOT NULL,
  `EngineRest` varchar(255) NOT NULL,
  `TrOhaul` varchar(255) NOT NULL,
  `PropOhaul` varchar(255) NOT NULL,
  `ApuOhaul` varchar(255) NOT NULL,
  `DynComp` varchar(255) NOT NULL,
  `TotalVariableCosts` varchar(255) NOT NULL,
  `CabinHgt` varchar(255) NOT NULL,
  `CabinWidth` varchar(255) NOT NULL,
  `CabinLength` varchar(255) NOT NULL,
  `CabinVol` varchar(255) NOT NULL,
  `DoorHeight` varchar(255) NOT NULL,
  `DoorWidth` varchar(255) NOT NULL,
  `HServ` varchar(255) NOT NULL,
  `CertCeiling` varchar(255) NOT NULL,
  `ServOei` varchar(255) NOT NULL,
  `BagVolInt` varchar(255) NOT NULL,
  `BagVolExt` varchar(255) NOT NULL,
  `PilotSeats` varchar(255) NOT NULL,
  `PassSeats` varchar(255) NOT NULL,
  `WtMTOGW` varchar(255) NOT NULL,
  `WtMaxLand` varchar(255) NOT NULL,
  `WtBow` varchar(255) NOT NULL,
  `WtUsableFuel` varchar(255) NOT NULL,
  `WtPayFuel` varchar(255) NOT NULL,
  `PayloadMax` varchar(255) NOT NULL,
  `RngNBAA` varchar(255) NOT NULL,
  `RngTnkFull` varchar(255) NOT NULL,
  `BFLMTOW` varchar(255) NOT NULL,
  `LandFar91` varchar(255) NOT NULL,
  `LandFar121` varchar(255) NOT NULL,
  `LandFar135` varchar(255) NOT NULL,
  `RocAllEng` varchar(255) NOT NULL,
  `RocOei` varchar(255) NOT NULL,
  `VcrMax` varchar(255) NOT NULL,
  `VcrNorm` varchar(255) NOT NULL,
  `VcrLngRng` varchar(255) NOT NULL,
  `AquisitionCost` varchar(255) NOT NULL,
  `PriceNew` varchar(255) NOT NULL,
  `PriceUsedMin` varchar(255) NOT NULL,
  `PriceUsedMax` varchar(255) NOT NULL,
  `InProd` varchar(255) NOT NULL,
  `BegProdYr` varchar(255) NOT NULL,
  `ProdYr` varchar(255) NOT NULL,
  `NumEng` varchar(255) NOT NULL,
  `EngMfgName` varchar(255) NOT NULL,
  `EngMod` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) $charset_collate
SQL;


        return dbDelta( $planes_2021_sql );

    }

    /**
     * @return array
     */
    private static function create_planes_prop_2021_table() {

        global $wpdb;

        $prefix          = $wpdb->base_prefix;
        $table_name      = $prefix . 'planes_prop_2021';
        $charset_collate = $wpdb->get_charset_collate();

        $planes_prop_2021_sql = <<<SQL
CREATE TABLE `$table_name` (
  `ID` int(24) NOT NULL AUTO_INCREMENT,
  `AcMgfName` varchar(255) NOT NULL,
  `AcName` varchar(255) NOT NULL,
  `GroupName` varchar(255) NOT NULL,
  `FuelFlow` varchar(255) NOT NULL,
  `FuelCostGal` varchar(255) NOT NULL,
  `MaintLabor` varchar(255) NOT NULL,
  `LaborCost` varchar(255) NOT NULL,
  `Parts` varchar(255) NOT NULL,
  `EngineRest` varchar(255) NOT NULL,
  `PropOhaul` varchar(255) NOT NULL,
  `ApuOhaul` varchar(255) NOT NULL,
  `DynComp` varchar(255) NOT NULL,
  `TotalVariableCosts` varchar(255) NOT NULL,
  `CabinHgt` varchar(255) NOT NULL,
  `CabinWidth` varchar(255) NOT NULL,
  `CabinLength` varchar(255) NOT NULL,
  `CabinVol` varchar(255) NOT NULL,
  `DoorHeight` varchar(255) NOT NULL,
  `DoorWidth` varchar(255) NOT NULL,
  `HServ` varchar(255) NOT NULL,
  `CertCeiling` varchar(255) NOT NULL,
  `ServOei` varchar(255) NOT NULL,
  `BagVolInt` varchar(255) NOT NULL,
  `BagVolExt` varchar(255) NOT NULL,
  `PilotSeats` varchar(255) NOT NULL,
  `PassSeats` varchar(255) NOT NULL,
  `WtMTOGW` varchar(255) NOT NULL,
  `WtMaxLand` varchar(255) NOT NULL,
  `WtBow` varchar(255) NOT NULL,
  `WtUsableFuel` varchar(255) NOT NULL,
  `WtPayFuel` varchar(255) NOT NULL,
  `PayloadMax` varchar(255) NOT NULL,
  `RngNBAA` varchar(255) NOT NULL,
  `RngTnkFull` varchar(255) NOT NULL,
  `BFLMTOW` varchar(255) NOT NULL,
  `LandFar91` varchar(255) NOT NULL,
  `LandFar121` varchar(255) NOT NULL,
  `LandFar135` varchar(255) NOT NULL,
  `RocAllEng` varchar(255) NOT NULL,
  `RocOei` varchar(255) NOT NULL,
  `VcrMax` varchar(255) NOT NULL,
  `VcrNorm` varchar(255) NOT NULL,
  `VcrLngRng` varchar(255) NOT NULL,
  `AquisitionCost` varchar(255) NOT NULL,
  `PriceNew` varchar(255) NOT NULL,
  `PriceUsedMin` varchar(255) NOT NULL,
  `PriceUsedMax` varchar(255) NOT NULL,
  `InProd` varchar(255) NOT NULL,
  `BegProdYr` varchar(255) NOT NULL,
  `ProdYr` varchar(255) NOT NULL,
  `NumEng` varchar(255) NOT NULL,
  `EngMfgName` varchar(255) NOT NULL,
  `EngMod` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) $charset_collate
SQL;

        return dbDelta( $planes_prop_2021_sql );

    }

}