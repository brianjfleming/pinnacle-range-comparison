<?php


namespace PinnacleAviation\Plugins\RangeComparison\DB;

class DB_Methods {

    /**
     * Query our Map Cities Custom Post Type for all records belonging to a provided continent.
     *
     * @param string $continent     Which continent we want.
     *
     * @return array|false
     */
    public static function get_cities_by_continent( $continent ) {

        $args = array(
            'post_type' => MAP_CITIES_POST_TYPE,    // Query our Map Cities Custom Post Type.
            'posts_per_page' => -1,                 // Get Every Post which meets our criteria.
            'meta_query' => array(
                array(
                    'key' => '_continent',
                    'value' => $continent
                )
            ),
	        'orderby' => 'post_title',
	        'order'   => 'ASC'
        );

        $query = new \WP_Query($args);

        if( $query->have_posts() ) {
            return $query->posts;
        } else {
            return false;
        }

    }

    /**
     * Outputs links for our range comparison map of all cities (based on given continent)
     *
     * @param string $continent     Which continent we want city links for.
     *                              Options are: north_america, south_america, asia, europe, africa, australia
     */
    public static function output_city_links_by_continent( $continent ) {

        // Query our Cities by continent.
        $cities = self::get_cities_by_continent( $continent );

        if( ! $cities ) {
            echo 'No Cities Found';
            return;
        }

        foreach( $cities as $city ) {

            $hq = '1' == get_post_meta( $city->ID, '_hq', true ) ? 'true' : 'false';

            echo wp_sprintf(
                '<a class="city-select" data-primary="%s" data-lat="%s" data-lng="%s" data-id="%s">%s</a>',
                $hq,
                get_post_meta( $city->ID, '_lat', true ),
                get_post_meta( $city->ID, '_lng', true ),
                $city->ID,
                $city->post_title
            );

        }

        return;

    }

    /**
     * Gets Jets.
     *
     * @return array Returns an array of obects matching our query.
     */
    public static function get_jets( $type = 'map', $group = 'all' ) {

		// Optional limiter for IDs.
	    $limit_clause = '';
	    $limit_ids    = array();

	    // If we are doing for Charter, get unique list of DB IDs for props.
	    if( 'charter' === $group ) {

		    $args = array(
			    'posts_per_page' => '-1',
			    'post_status' => 'publish',
			    'post_type' => 'charter-fleets',
			    'meta_query' => array(
				    array(
					    'key' => 'aircraft_link_jet',
					    'compare' => 'EXISTS'
				    )
			    )
		    );

		    $wp_query = new \WP_Query( $args );

		    foreach( $wp_query->posts as $post ) {
			    $this_att = get_post_meta( $post->ID, 'aircraft_link_jet', true );

			    if( ! empty( $this_att ) ) {
				    $limit_ids[] = $this_att;
			    }
		    }

		    wp_reset_postdata();

		    $limit_clause = "AND `ID` IN (" . implode( ',', $limit_ids ) . ")";

	    }

	    global $wpdb;

    	if( 'map' === $type ) {
		    $select_fields = 'RngNBAA, RngTnkFull, AcMgfName, AcName';
	    } else if( 'comparison' === $type ) {
    		$select_fields = 'ID, AcMgfName, AcName';
	    }

	    $prefix          = $wpdb->base_prefix;
	    $table_name = $prefix . 'planes_2021';

        $query = <<<SQL
SELECT $select_fields
  FROM `$table_name` 
  WHERE 
  `RngNBAA` != '' AND 
  `RngTnkFull` != '' 
  $limit_clause
  ORDER BY `AcMgfName` ASC
SQL;

        return $wpdb->get_results( $query );
    }

    /**
     * Get Props.
     *
     * @return array Returns an array of obects matching our query.
     */
    public static function get_props( $type = 'map', $group = 'all' ) {

	    // Optional limiter for IDs.
	    $limit_clause = '';
	    $limit_ids    = array();

	    // If we are doing for Charter, get unique list of DB IDs for props.
	    if( 'charter' === $group ) {

		    $args = array(
			    'posts_per_page' => '-1',
			    'post_status' => 'publish',
			    'post_type' => 'charter-fleets',
			    'meta_query' => array(
				    array(
					    'key' => 'aircraft_link_jet',
					    'compare' => 'EXISTS'
				    )
			    )
		    );
		    $wp_query = new \WP_Query( $args );

		    foreach( $wp_query->posts as $post ) {
			    $this_att = get_post_meta( $post->ID, 'aircraft_link_prop', true );

			    if( ! empty( $this_att ) ) {
				    $limit_ids[] = $this_att;
			    }
		    }

		    wp_reset_postdata();

		    $limit_clause = "AND `ID` IN (" . implode( ',', $limit_ids ) . ")";

	    }

	    if( 'map' === $type ) {
		    $select_fields = 'RngNBAA, RngTnkFull, AcMgfName, AcName';
	    } else if( 'comparison' === $type ) {
		    $select_fields = 'ID, AcMgfName, AcName';
	    }

	    global $wpdb;
	    $prefix     = $wpdb->base_prefix;
	    $table_name = $prefix . 'planes_prop_2021';

        $query = <<<SQL
SELECT $select_fields
  FROM `$table_name` 
  WHERE 
  `RngNBAA` != '' AND 
  `RngTnkFull` != '' 
  $limit_clause
  ORDER BY `AcMgfName` ASC
SQL;

        global $wpdb;
        return $wpdb->get_results( $query );
    }

    /**
     * Output link of aircraft for our range comparison map.
     *
     * @param string $category      jets|props
     */
    public static function output_range_links( $category = 'jets', $group = 'all' ) {

        switch( $category ) {
            case "props":
                $results = self::get_props( 'map', $group );
                break;
            case "jets":
                $results = self::get_jets( 'map', $group );
                break;
            default:
                $results = array();
                break;
        }

        // Always need an option for 'None'
        $output[] = "<a class='plane-select' data-r1='0' data-r2='0' data-r3='0' data-id=''>None</a>";

        if( empty( $results ) ) {
            echo 'No Aircraft Found';
        }

        foreach( $results as $index => $aircraft ) {

            // Ranges.
            $r1 = preg_replace("/[^0-9]/","",$aircraft->RngNBAA);
            $r2 = false;
            $r3 = preg_replace("/[^0-9]/","",$aircraft->RngTnkFull);

            // Generate Random ID.
            $master = substr(md5(uniqid(mt_rand(), true)), 0, 8);
            $output[] = "<a class='plane-select' data-r1='{$r1}' data-r2='{$r2}' data-r3='{$r3}' data-id='{$master}'>{$aircraft->AcMgfName} {$aircraft->AcName}</a>";

        }

        echo implode("\r\n", $output );
    }

	/**
	 * @param int $id   - ID of the plane
	 * @param string $category 'props'|'jets' - category plane is in.
	 * @return mixed
	 */
    public static function get_aircraft_details( $id, $category ) {


	    global $wpdb;
	    $prefix = $wpdb->base_prefix;

    	switch( $category ) {
		    case "props":
		    	$table_name = $prefix . 'planes_prop_2021';
		    	break;
		    case "jets":
		    default:
			    $table_name = $prefix . 'planes_2021';
		    	break;
	    }

		    $query = <<<SQL
SELECT *
  FROM `$table_name` 
  WHERE ID = '$id'
SQL;

		    global $wpdb;
		    return $wpdb->get_row( $query );

    }

}