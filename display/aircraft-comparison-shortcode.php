<?php
/**
 * Shortcode callback for [range_comparison_map]
 *
 * @package PinnacleAviation\Plugins\RangeComparison
 */

namespace PinnacleAviation\Plugins\RangeComparison\Display;

/**
 * @return string         Shortcode Output.
 */
function aircraft_comparison_shortcode() {

    return Common::aircraft_comparison_markup();

}