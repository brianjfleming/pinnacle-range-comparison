<?php

namespace PinnacleAviation\Plugins\RangeComparison\Display\Ajax;

use PinnacleAviation\Plugins\RangeComparison\DB\DB_Methods;

function get_dropdown_options() {

	// We need our dB methods.
	require_once RANGE_COMPARISON_DIR . '/db/class.db-methods.php';

	switch( $_POST['category'] ) {
		case "jets":
			$planes = DB_Methods::get_jets( 'comparison' );
			break;
		case "props":
			$planes = DB_Methods::get_props( 'comparison' );
			break;
		default:
			$planes = null;
			break;
	}

	if( null !== $planes ) {
		wp_send_json( $planes );
	} else {
		wp_send_json_error( $_POST );
	}

}

function get_plane_details() {

	require_once RANGE_COMPARISON_DIR . '/db/class.db-methods.php';

	wp_send_json( DB_Methods::get_aircraft_details( $_POST['plane_id'], $_POST['category'] ) );

}