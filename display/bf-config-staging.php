<?php


define('WP_DEBUG', true);
define( 'SAVEQUERIES', true);

/**
 * Content Repo Blog ID - The blog ID of the child site which is the content repo for this network.
 */
define('BFCR_REPO_BLOG_ID', 1255 );

/**
 * Content Marketing Blog ID - The blog ID of the child site which is the content marketing repo for this network.
 */
define('BFCM_BLOG_ID', 2939 );

/**
 * Content Marketing URL - The URL of the child site which is the content marketing repo for this network.
 */
define('BF_CONTENT_MARKETING_URL', 'https://insurance-development.brightfiregroup.com/insuranceneighborcom/');

// Redirect traffic if it doesn't originate from one of the whitelisted countries
$geoip_country = getenv('HTTP_GEOIP_COUNTRY_CODE');
$bf_geoip_whitelist = array( 'US', 'CA', 'GB', 'IE', 'AU');

if ( $geoip_country && ! in_array( $geoip_country, $bf_geoip_whitelist ) ) {
  header( 'Location: https://www.insurancedatacenter.com/404.html' );
  exit;
}

// define( 'AUTOBLOG_POST_DUPLICATE_CHECK', 'guid');

// define( 'AUTOBLOG_PROCESSING_TIMELIMIT', 20);
// define( 'AUTOBLOG_FORCE_PROCESS_ALL', true);

define('BF_ENV', 'STG');
//define('DISABLE_WP_CRON', false);
define('BF_DEV_ADMIN_EMAIL', 'bob+test@brightfire.com');

// This file should not be committed to repo but should be saved directly on server via SFTP
include( dirname( __FILE__) . '/bf-licenses.php' );

// These settings require the BrightFire SMTP mail plugin activated
define( 'BF_SMTP_USER', 'no-reply@brightfiremail.com' );
define( 'BF_SMTP_PASS', 'Wzi9dBM6nipoxuDo' );
define( 'BF_SMTP_FROM_NAME', 'BrightFire Insurance Websites' );

// Insurance network specific constants
// define('SUNRISE', 'on' ); // we need domain mapping no matter what

define('FACEBOOK_APP_SECRET', '3a4d76cc12b48b2c43ce961e96cb5b4d'); // Live Facebook App
//define('BF_ADMIN_NOTICE', 'WPENGINE'); // DEV or WPENGINE

// define('FORCE_SSL_ADMIN', true);
// define('DISABLE_WP_CRON', 'true');
