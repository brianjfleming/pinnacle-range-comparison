<?php
/**
 * Widget Class for output Widget
 */

namespace PinnacleAviation\Plugins\RangeComparison\Display;

/**
 * Class Range_Comparison_Widget
 * @package PinnacleAviation\Plugins\RangeComparison
 */
class Aircraft_Comparison extends \WP_Widget {

    /**
     * Range_Comparison_Widget constructor.
     */
    function __construct() {
        parent::__construct(
            'aircraft_comparison',
            'Aircraft Comparison',
            array(
                'description' => 'Outputs JS/GMaps Range Comparison Map'
            )
        );

        /**
         * Required Scripts and Styles
         */
        add_action( 'wp_enqueue_scripts', function() {
            wp_enqueue_script( 'aircraft-comparison-scripts' );
            wp_enqueue_style( 'aircraft-comparison-styles' );

            wp_localize_script( 'aircraft-comparison-scripts', 'data', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
        });
    }

    /**
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance ) {

        echo Common::aircraft_comparison_markup();

    }

    /**
     * @param array $instance
     * @return string|void
     */
    public function form( $instance ) {

        echo '<p>No Options Here.</p>';

    }

    /**
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update( $new_instance, $old_instance ) {
        return $old_instance;
    }

// Class Range_Comparison_Widget ends here
}