<?php


namespace PinnacleAviation\Plugins\RangeComparison\Display;

use PinnacleAviation\Plugins\RangeComparison\DB\DB_Methods;

/**
 * Class Common
 * @package PinnacleAviation\Plugins\RangeComparison
 */
class Common {

	/**
	 * @return array
	 */
    public static function range_comparison_map_default_atts() {
        return array(
            'width'  => '100%',
            'height' => '700px',
            'group'  => 'all'   // 'all' or 'charter' - if charter, this will only pull in jets and props which are present
                                // in the charter fleets post type and linked to an aircraft.
        );
    }

	/**
	 * @param $atts
	 * @return false|string
	 */
    public static function range_comparison_map_markup( $atts ) {

        // We need our dB methods.
        require_once RANGE_COMPARISON_DIR . '/db/class.db-methods.php';

        $atts = shortcode_atts(
            self::range_comparison_map_default_atts(),
            $atts
        );

        ob_start();
        ?><h2 id="range_finder_mobile">Please Use A Desktop <br> To Use the Range Tool</h2>
        <div id="range_finder" style="width: <?php echo $atts['width']; ?>; height: <?php echo $atts['height']; ?>;">
            <div id="key"><img src="<?php echo RANGE_COMPARISON_DIST; ?>images/range-key.jpg"></div>
            <div id="fullscreen"><a class="map-fullscreen"><i class="fas fa-expand-arrows-alt"></i>  Fullscreen</a></div>
            <div id="map-loading"><img src="<?php echo RANGE_COMPARISON_DIST; ?>images/map-loading.gif"></div>
            <!--- START MAP API --->
            <!--- /////////////////////////////////////////////// --->
            <div id="range_prompt">
                <a rel="continent_dd" class="dropdown_trigger">Select Continent <span>To begin, please select a continent</span></a>
            </div>
            <div id="range_nav">
                <a rel="continent_dd" class="dropdown_trigger">Select Continent <span id="continent_selection">(none selected)</span></a>
                <a rel="city_dd" class="dropdown_trigger">Select City <span id="city_selection">(none selected)</span></a>
                <a rel="plane_a_dd" class="dropdown_trigger">Aircraft 1 <span id="plane_a_selection">(none selected)</span></a>
                <a rel="plane_b_dd" class="dropdown_trigger">Aircraft 2 <span id="plane_b_selection">(none selected)</span></a>
                <a rel="plane_c_dd" class="dropdown_trigger">Aircraft 3 <span id="plane_c_selection">(none selected)</span></a>
                <div class="clear"></div>
            </div>

            <div id="continent_dd" class="range_dropdown">
                <a class="continent-select" rel="africa">Africa</a>
                <a class="continent-select" rel="asia">Asia</a>
                <a class="continent-select" rel="australia">Australia</a>
                <a class="continent-select" rel="europe">Europe</a>
                <a class="continent-select" rel="north_america">North America</a>
                <a class="continent-select" rel="south_america">South America</a>
            </div>

            <div id="city_dd" class="range_dropdown">
                <div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
                <div class="viewport">
                    <div class="overview">
                        <div id="city_africa" class="city_group">
                            <?php DB_Methods::output_city_links_by_continent( 'africa' ); ?>
                        </div>
                        <div id="city_asia" class="city_group">
                            <?php DB_Methods::output_city_links_by_continent( 'asia' ); ?>
                        </div>
                        <div id="city_australia" class="city_group">
                            <?php DB_Methods::output_city_links_by_continent( 'australia' ); ?>
                        </div>
                        <div id="city_europe" class="city_group">
                            <?php DB_Methods::output_city_links_by_continent( 'europe' ); ?>
                        </div>
                        <div id="city_north_america" class="city_group">
                            <?php DB_Methods::output_city_links_by_continent( 'north_america' ); ?>
                        </div>
                        <div id="city_south_america" class="city_group">
                            <?php DB_Methods::output_city_links_by_continent( 'south_america' ); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div id="plane_a_dd" class="range_dropdown">
                <div class="selectors">
                    <a class="tab-select tab_1 active" id="jets_1_trigger" data-category="jets" data-tab="1">Jets</a>
                    <a class="tab-select tab_1" id="props_1_trigger" data-category="props" data-tab="1">Turboprops</a>
                    <div class="clear"></div>
                </div>
                <div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
                <div class="viewport">
                    <div class="overview">
                        <div id="jets_1" class="cat_1">
                            <?php DB_Methods::output_range_links( 'jets', $atts['group'] ); ?>
                        </div>
                        <div id="props_1" class="cat_1" style="display: none;">
                            <?php DB_Methods::output_range_links( 'props', $atts['group'] ); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div id="plane_b_dd" class="range_dropdown">
                <div class="selectors">
                    <a class="tab-select tab_2 active" id="jets_2_trigger" data-category="jets" data-tab="2">Jets</a>
                    <a class="tab-select tab_2" id="props_2_trigger" data-category="props" data-tab="2">Turboprops</a>
                    <div class="clear"></div>
                </div>
                <div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
                <div class="viewport">
                    <div class="overview">
                        <div id="jets_2" class="cat_2">
                            <?php DB_Methods::output_range_links( 'jets', $atts['group'] ); ?>
                        </div>
                        <div id="props_2" class="cat_2" style="display: none;">
                            <?php DB_Methods::output_range_links( 'props', $atts['group'] ); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div id="plane_c_dd" class="range_dropdown">
                <div class="selectors">
                    <a class="tab-select tab_3 active" id="jets_3_trigger" data-category="jets" data-tab="3">Jets</a>
                    <a class="tab-select tab_3" id="props_3_trigger" data-category="props" data-tab="3">Turboprops</a>
                    <div class="clear"></div>
                </div>
                <div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
                <div class="viewport">
                    <div class="overview">
                        <div id="jets_3" class="cat_3">
                            <?php DB_Methods::output_range_links( 'jets', $atts['group'] ); ?>
                        </div>
                        <div id="props_3" class="cat_3" style="display: none;">
                            <?php DB_Methods::output_range_links( 'props', $atts['group'] ); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div id="map_canvas" style="width: 100%; height: calc(<?php echo $atts['height']; ?> - 45px); z-index: 99"></div>
            <!--- /////////////////////////////////////////////// --->
            <!--- END MAP API --->
        </div>
        <div id="range_finder_marker"></div><?php
        $output = ob_get_contents();
        ob_end_clean();
        return $output;

    }

	/**
	 * @return array
	 */
    public static function map_options_default_attributes() {
        return array(
            'label' => 'Select Range to Show:',
            'r3_label' => 'Maximum Range',
            'r1_label' => 'All Seats Full'
        );
    }

    /**
     * @param $atts
     *
     * @return string
     */
    public static function map_options_markup( $atts ) {

        $atts = shortcode_atts(
            self::map_options_default_attributes(),
            $atts
        );

        $output = array();

        $output[] = "<div class='map-options' id='map_options'>";
        $output[] = "<span class='option-heading'>{$atts['label']}</span>";
        $output[] = "<select name='range-option' id='range-option' class='range-option'>";
        $output[] = "<option value='r3'>{$atts['r3_label']}</option>";
	    $output[] = "<option value='r1'>{$atts['r1_label']}</option>";
        $output[] = "</select>";
        $output[] = "<input type='button' class='range-hard-reset' id='range-hard-reset' value='Reset All'>";
        $output[] = "</div>"; // map-options.

        $output[] = "<div id='map_options_marker'></div>";

        return implode("\r\n", $output );

    }

	/**
	 * Output for the 'Aircraft Comparison' Table.
	 */
    public static function aircraft_comparison_markup() {

        ob_start();

        ?>

        <div id="aircraft_comparison">

            <div class="aircraft_container aircraft_1">
                <select class="aircraft_category_select" data-comp-id="1" style="font-weight: 700; margin-bottom: 0.5em;">
                    <option value="">Select a Category</option>
                    <option value="jets">Jets</option>
                    <option value="props">Turboprops</option>
                </select>
                <select class="aircraft_select" disabled data-comp-id="1"><option>Select an Aircraft</option></select>
            </div>
            <div class="aircraft_container aircraft_2">
                <select class="aircraft_category_select" data-comp-id="2" style="font-weight: 700; margin-bottom: 0.5em;">
                    <option value="">Select a Category</option>
                    <option value="jets">Jets</option>
                    <option value="props">Turboprops</option>
                </select>
                <select class="aircraft_select" disabled data-comp-id="2"><option>Select an Aircraft</option></select>
            </div>
        </div>

        <table id="comparison_table">
            <tr>
                <td id="comp1"><h5 class="comp-title comparison_subtable comp-1">
                        <span data-props="1-AcMgfName" data-jets="1-AcMgfName"></span> <span data-props="1-AcName" data-jets="1-AcName"></span>
                    </h5></td>
                <td class="comp_mid_row"></td>
                <td id="comp2"><h5 class="comp-title comparison_subtable comp-2">
                        <span data-props="2-AcMgfName" data-jets="2-AcMgfName"></span> <span data-props="2-AcName" data-jets="2-AcName"></span>
                    </h5></td>
            </tr>
            <tr class="split">
                <td>
                    <table class="comparison_subtable comp-1">
                        <tr>
                            <td colspan="2" class="comp_subheading">
                                Average Direct Operating Cost (DOC) Per Hour
                            </td>
                        </tr>
                        <tr>
                            <td class="comp_label">Fuel</td>
                            <td class="comp_data format_dollars" data-props="1-FuelCostGal" data-jets="1-FuelCostGal">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Maintenance Labor</td>
                            <td class="comp_data format_dollars" data-props="1-MaintLabor" data-jets="1-MaintLabor">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Parts</td>
                            <td class="comp_data format_dollars" data-props="1-Parts" data-jets="1-Parts">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Engine Reserve</td>
                            <td class="comp_data format_dollars" data-props="1-EngineRest" data-jets="1-EngineRest">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label 1-thrust-reverser">Thrust Reverser</td>
                            <td class="comp_data format_dollars" data-props="1-PropOhaul" data-jets="1-TrOhaul">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">APU Reserve</td>
                            <td class="comp_data format_dollars" data-props="1-ApuOhaul" data-jets="1-ApuOhaul">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Major Periodic Maintenance</td>
                            <td class="comp_data format_dollars" data-props="1-MajorPeriodicMaint" data-jets="1-MajorPeriodicMaint">--</td>
                        </tr>
                        <tr>
                            <td class="total">Total DOC Per Hour</td>
                            <td class="comp_data total format_dollars" style="font-weight: 700; width: 70px;" data-props="1-TotalVariableCosts" data-jets="1-TotalVariableCosts">--</td>
                        </tr>
                    </table>
                </td>
                <td class="comp_mid_row"></td>
                <td>
                    <table class="comparison_subtable comp-2">
                        <tr>
                            <td colspan="2" class="comp_subheading">
                                Average Direct Operating Cost (DOC) Per Hour
                            </td>
                        </tr>
                        <tr>
                            <td class="comp_label">Fuel</td>
                            <td class="comp_data format_dollars" data-props="2-FuelCostGal" data-jets="2-FuelCostGal">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Maintenance Labor</td>
                            <td class="comp_data format_dollars" data-props="2-MaintLabor" data-jets="2-MaintLabor">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Parts</td>
                            <td class="comp_data format_dollars" data-props="2-Parts" data-jets="2-Parts">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Engine Reserve</td>
                            <td class="comp_data format_dollars" data-props="2-EngineRest" data-jets="2-EngineRest">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label 2-thrust-reverser">Thrust Reverser</td>
                            <td class="comp_data format_dollars" data-props="2-PropOhaul" data-jets="2-TrOhaul">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">APU Reserve</td>
                            <td class="comp_data format_dollars" data-props="2-ApuOhaul" data-jets="2-ApuOhaul">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Major Periodic Maintenance</td>
                            <td class="comp_data format_dollars" data-props="2-MajorPeriodicMaint" data-jets="2-MajorPeriodicMaint">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label total">Total DOC Per Hour</td>
                            <td class="comp_data total format_dollars" style="font-weight: 700; width: 70px;" data-props="2-TotalVariableCosts" data-jets="2-TotalVariableCosts">--</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="split">
                <td>
                    <table class="comparison_subtable comp-1">
                        <tr>
                            <td colspan="2" class="comp_subheading">Factors</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Fuel Flow</td>
                            <td class="comp_data add_suffix" data-props="1-FuelFlow" data-jets="1-FuelFlow" data-suffix="gph">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Fuel Price</td>
                            <td class="comp_data format-dollars hardcoded">$<? echo get_option('lr_fuel_cost'); ?>/gal</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Labor Rate</td>
                            <td class="comp_data format-dollars hardcoded">
                                <span class="1-jets 1-labor-rate">$<? echo get_option('lr_jets_cost'); ?>/hr</span>
                                <span class="1-props 1-labor-rate">$<? echo get_option('lr_prop_cost'); ?>/hr</span>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="comp_mid_row"></td>
                <td>
                    <table class="comparison_subtable comp-2">
                        <tr>
                            <td colspan="2" class="comp_subheading">Factors</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Fuel Flow</td>
                            <td class="comp_data" data-props="2-FuelFlow" data-jets="2-FuelFlow" data-suffix="gph">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Fuel Price</td>
                            <td class="comp_data hardcoded format-dollars">$<? echo get_option('lr_fuel_cost'); ?>/gal</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Labor Rate</td>
                            <td class="comp_data hardcoded format-dollars">
                                <span class="2-jets 2-labor-rate">$<? echo get_option('lr_jets_cost'); ?>/hr</span>
                                <span class="2-props 2-labor-rate">$<? echo get_option('lr_prop_cost'); ?>/hr</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="split">
                <td>
                    <table class="comparison_subtable comp-1">
                        <tr>
                            <td colspan="2" class="comp_subheading">Aircraft Performance</td>
                        </tr>
                        <tr class="highlight">
                            <td class="comp_label">Maximum Range</td>
                            <td class="comp_data" data-props="1-RngTnkFull" data-jets="1-RngTnkFull" data-suffix="nm">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Range - Seats Full</td>
                            <td class="comp_data" data-props="1-RngNBAA" data-jets="1-RngNBAA" data-suffix="nm">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Balanced Field Length</td>
                            <td class="comp_data" data-props="1-BFLMTOW" data-jets="1-BFLMTOW" data-suffix="ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Landing Distance (FAR 91)</td>
                            <td class="comp_data" data-props="1-LandFar91" data-jets="1-LandFar91" data-suffix="ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Landing Distance (FAR 135)</td>
                            <td class="comp_data" data-props="1-LandFar135" data-jets="1-LandFar135" data-suffix="ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Rate of Climb - All Engines</td>
                            <td class="comp_data" data-props="1-RocAllEng" data-jets="1-RocAllEng" data-suffix="ft/min">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Rate of Climb - One Engine Inop</td>
                            <td class="comp_data" data-props="1-RocOei" data-jets="1-RocOei" data-suffix="ft/min">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Maximum Cruise Speed</td>
                            <td class="comp_data" data-props="1-VcrMax" data-jets="1-VcrMax" data-suffix="kts">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Normal Cruise Speed</td>
                            <td class="comp_data" data-props="1-VcrNorm" data-jets="1-VcrNorm" data-suffix="kts">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Long Range Cruise Speed</td>
                            <td class="comp_data" data-props="1-VcrLngRng" data-jets="1-VcrLngRng" data-suffix="kts">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Certified Ceiling</td>
                            <td class="comp_data" data-props="1-CertCeiling" data-jets="1-CertCeiling" data-suffix="ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Service Ceiling</td>
                            <td class="comp_data" data-props="1-HServ" data-jets="1-HServ" data-suffix="ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Service Ceiling - One Engine Inop</td>
                            <td class="comp_data" data-props="1-ServOei" data-jets="1-ServOei" data-suffix="ft">--</td>
                        </tr>
                    </table>
                </td>
                <td class="comp_mid_row"></td>
                <td>
                    <table class="comparison_subtable comp-2">
                        <tr>
                            <td colspan="2" class="comp_subheading">Aircraft Performance</td>
                        </tr>
                        <tr class="highlight">
                            <td class="comp_label">Maximum Range</td>
                            <td class="comp_data" data-props="2-RngTnkFull" data-jets="2-RngTnkFull" data-suffix="nm">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Range - Seats Full</td>
                            <td class="comp_data" data-props="2-RngNBAA" data-jets="2-RngNBAA" data-suffix="nm">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Balanced Field Length</td>
                            <td class="comp_data" data-props="2-BFLMTOW" data-jets="2-BFLMTOW" data-suffix="ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Landing Distance (FAR 91)</td>
                            <td class="comp_data" data-props="2-LandFar91" data-jets="2-LandFar91" data-suffix="ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Landing Distance (FAR 135)</td>
                            <td class="comp_data" data-props="2-LandFar135" data-jets="2-LandFar135" data-suffix="ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Rate of Climb - All Engines</td>
                            <td class="comp_data" data-props="2-RocAllEng" data-jets="2-RocAllEng" data-suffix="ft/min">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Rate of Climb - One Engine Inop</td>
                            <td class="comp_data" data-props="2-RocOei" data-jets="2-RocOei" data-suffix="ft/min">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Maximum Cruise Speed</td>
                            <td class="comp_data" data-props="2-VcrMax" data-jets="2-VcrMax" data-suffix="kts">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Normal Cruise Speed</td>
                            <td class="comp_data" data-props="2-VcrNorm" data-jets="2-VcrNorm" data-suffix="kts">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Long Range Cruise Speed</td>
                            <td class="comp_data" data-props="2-VcrLngRng" data-jets="2-VcrLngRng" data-suffix="kts">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Certified Ceiling</td>
                            <td class="comp_data" data-props="2-CertCeiling" data-jets="2-CertCeiling" data-suffix="ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Service Ceiling</td>
                            <td class="comp_data" data-props="2-HServ" data-jets="2-HServ" data-suffix="ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Service Ceiling - One Engine Inop</td>
                            <td class="comp_data" data-props="2-ServOei" data-jets="2-ServOei" data-suffix="ft">--</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="split">
                <td>
                    <table class="comparison_subtable comp-1">
                        <tr>
                            <td colspan="2" class="comp_subheading">Aircraft Specifications</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Engine Manufacturer</td>
                            <td class="comp_data" data-props="1-EngMfgName" data-jets="1-EngMfgName">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Engine Model</td>
                            <td class="comp_data" data-props="1-EngMod" data-jets="1-EngMod">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Cabin Height</td>
                            <td class="comp_data" data-props="1-CabinHgt" data-jets="1-CabinHgt" data-suffix="ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Cabin Width</td>
                            <td class="comp_data" data-props="1-CabinWidth" data-jets="1-CabinWidth" data-suffix="ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Cabin Length</td>
                            <td class="comp_data" data-props="1-CabinLength" data-jets="1-CabinLength" data-suffix="ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Cabin Door Height</td>
                            <td class="comp_data" data-props="1-DoorHeight" data-jets="1-DoorHeight" data-suffix="ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Cabin Door Width</td>
                            <td class="comp_data" data-props="1-DoorWidth" data-jets="1-DoorWidth" data-suffix="ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Cabin Volume</td>
                            <td class="comp_data" data-props="1-CabinVol" data-jets="1-CabinVol" data-suffix="cu ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Baggage Volume Internal</td>
                            <td class="comp_data" data-props="1-BagVolInt" data-jets="1-BagVolInt" data-suffix="cu ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Baggage Volume External</td>
                            <td class="comp_data" data-props="1-BagVolExt" data-jets="1-BagVolExt" data-suffix="cu ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Seats - Executive</td>
                            <td class="comp_data" data-props="1-PassSeats" data-jets="1-PassSeats">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Exterior Length</td>
                            <td class="comp_data" data-props="1-exterior_length" data-jets="1-exterior_length" data-suffix="ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Exterior Width</td>
                            <td class="comp_data" data-props="1-exterior_width" data-jets="1-exterior_width" data-suffix="ft">--</td>
                        </tr>
                    </table>
                </td>
                <td class="comp_mid_row"></td>
                <td>
                    <table class="comparison_subtable comp-2">
                        <tr>
                            <td colspan="2" class="comp_subheading">Aircraft Specifications</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Engine Manufacturer</td>
                            <td class="comp_data" data-props="2-EngMfgName" data-jets="2-EngMfgName">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Engine Model</td>
                            <td class="comp_data" data-props="2-EngMod" data-jets="2-EngMod">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Cabin Height</td>
                            <td class="comp_data" data-props="2-CabinHgt" data-jets="2-CabinHgt" data-suffix="ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Cabin Width</td>
                            <td class="comp_data" data-props="2-CabinWidth" data-jets="2-CabinWidth" data-suffix="ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Cabin Length</td>
                            <td class="comp_data" data-props="2-CabinLength" data-jets="2-CabinLength" data-suffix="ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Cabin Door Height</td>
                            <td class="comp_data" data-props="2-DoorHeight" data-jets="2-DoorHeight" data-suffix="ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Cabin Door Width</td>
                            <td class="comp_data" data-props="2-DoorWidth" data-jets="2-DoorWidth" data-suffix="ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Cabin Volume</td>
                            <td class="comp_data" data-props="2-CabinVol" data-jets="2-CabinVol" data-suffix="cu ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Baggage Volume Internal</td>
                            <td class="comp_data" data-props="2-BagVolInt" data-jets="2-BagVolInt" data-suffix="cu ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Baggage Volume External</td>
                            <td class="comp_data" data-props="2-BagVolExt" data-jets="2-BagVolExt" data-suffix="cu ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Seats - Executive</td>
                            <td class="comp_data" data-props="2-PassSeats" data-jets="2-PassSeats">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Exterior Length</td>
                            <td class="comp_data" data-props="2-exterior_length" data-jets="2-exterior_length" data-suffix="ft">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Exterior Width</td>
                            <td class="comp_data" data-props="2-exterior_width" data-jets="2-exterior_width" data-suffix="ft">--</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="split">
                <td>
                    <table class="comparison_subtable comp-1">
                        <tr>
                            <td colspan="2" class="comp_subheading">Aircraft Weights</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Maximum Take-Off Weight</td>
                            <td class="comp_data" data-props="1-WtMTOGW" data-jets="1-WtMTOGW" data-suffix="lbs">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Maximum Landing Weight</td>
                            <td class="comp_data" data-props="1-WtMaxLand" data-jets="1-WtMaxLand" data-suffix="lbs">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Basic Operating Weight w/Crew</td>
                            <td class="comp_data" data-props="1-WtBow" data-jets="1-WtBow" data-suffix="lbs">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Usable Fuel</td>
                            <td class="comp_data" data-props="1-WtUsableFuel" data-jets="1-WtUsableFuel" data-suffix="lbs">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Payload with Full Fuel</td>
                            <td class="comp_data" data-props="1-WtPayFuel" data-jets="1-WtPayFuel" data-suffix="lbs">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Maximum Payload</td>
                            <td class="comp_data" data-props="1-PayloadMax" data-jets="1-PayloadMax" data-suffix="lbs">--</td>
                        </tr>
                    </table>
                </td>
                <td class="comp_mid_row"></td>
                <td>
                    <table class="comparison_subtable comp-2">
                        <tr>
                            <td colspan="2" class="comp_subheading">Aircraft Weights</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Maximum Take-Off Weight</td>
                            <td class="comp_data" data-props="2-WtMTOGW" data-jets="2-WtMTOGW" data-suffix="lbs">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Maximum Landing Weight</td>
                            <td class="comp_data" data-props="2-WtMaxLand" data-jets="2-WtMaxLand" data-suffix="lbs">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Basic Operating Weight w/Crew</td>
                            <td class="comp_data" data-props="2-WtBow" data-jets="2-WtBow" data-suffix="lbs">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Usable Fuel</td>
                            <td class="comp_data" data-props="2-WtUsableFuel" data-jets="2-WtUsableFuel" data-suffix="lbs">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Payload with Full Fuel</td>
                            <td class="comp_data" data-props="2-WtPayFuel" data-jets="2-WtPayFuel" data-suffix="lbs">--</td>
                        </tr>
                        <tr>
                            <td class="comp_label">Maximum Payload</td>
                            <td class="comp_data" data-props="2-PayloadMax" data-jets="2-PayloadMax" data-suffix="lbs">--</td>
                        </tr>
                    </table>
                </td>
            </tr>
            
        </table>

        <?php

	    $output = ob_get_contents();

	    ob_end_clean();

	    return $output;

    }

}