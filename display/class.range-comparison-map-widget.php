<?php
/**
 * Widget Class for output Widget
 */

namespace PinnacleAviation\Plugins\RangeComparison\Display;

/**
 * Class Range_Comparison_Widget
 * @package PinnacleAviation\Plugins\RangeComparison
 */
class Range_Comparison_Map extends \WP_Widget {

    /**
     * Range_Comparison_Widget constructor.
     */
    function __construct() {
        parent::__construct(
            'range_comparison_map',
            'Range Comparison Map',
            array(
                'description' => 'Outputs JS/GMaps Range Comparison Map'
            )
        );

        /**
         * Required Scripts and Styles
         */
        add_action( 'wp_enqueue_scripts', function() {
            wp_enqueue_style( 'fontawesome5' );
            wp_enqueue_script( 'range-comparison-scripts' );
            wp_enqueue_style( 'range-comparison-styles' );
            wp_enqueue_script( 'google-maps-jsapi' );
        });
    }

    /**
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance ) {

        echo Common::range_comparison_map_markup( array( 'height' => $instance['height'] . 'px' ) );

    }

    /**
     * @param array $instance
     * @return string|void
     */
    public function form( $instance ) {

        $height = ! empty( $instance['height'] ) ? $instance['height'] : '';
	    $group = ! empty( $instance['group'] ) ? $instance['group'] : '';

        ?>

        <p>
            <label for="<?php echo $this->get_field_id( 'height'); ?>">Map Height:</label>
            <input
                class="widefat"
                type="number"
                id="<?php echo $this->get_field_id( 'height' ); ?>"
                name="<?php echo $this->get_field_name( 'height' ); ?>"
                value="<?php echo esc_attr( $height ); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'group'); ?>">Group ('all' or 'charter'):</label>
            <input
                    class="widefat"
                    type="number"
                    id="<?php echo $this->get_field_id( 'group' ); ?>"
                    name="<?php echo $this->get_field_name( 'group' ); ?>"
                    value="<?php echo esc_attr( $group ); ?>" />
        </p>

        <?php

}

    /**
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update( $new_instance, $old_instance ) {

        $instance = $old_instance;
        $instance['height'] = strip_tags( $new_instance['height'] );
	    $instance['group'] = strip_tags( $new_instance['group'] );

        return $instance;
    }

// Class Range_Comparison_Widget ends here
}