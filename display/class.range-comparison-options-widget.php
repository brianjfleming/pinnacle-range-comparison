<?php
/**
 * Widget Class for output Widget
 */

namespace PinnacleAviation\Plugins\RangeComparison\Display;

/**
 * Class Range_Comparison_Widget
 * @package PinnacleAviation\Plugins\RangeComparison
 */
class Range_Comparison_Options extends \WP_Widget {

    /**
     * Range_Comparison_Widget constructor.
     */
    function __construct() {
        parent::__construct(
            'range_comparison_options',
            'Range Comparison Options',
            array(
                'description' => 'Outputs range selector options to connect with Range Comparison Map'
            )
        );
    }

    /**
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance ) {

        echo Common::map_options_markup( array( 'label' => $instance['label'] ) );

    }

    /**
     * @param array $instance
     * @return string|void
     */
    public function form( $instance ) {

        $label = ! empty( $instance['label'] ) ? $instance['label'] : '';

        ?>

        <p>
            <label for="<?php echo $this->get_field_id( 'label'); ?>">Selector Label:</label>
            <input
                class="widefat"
                type="text"
                id="<?php echo $this->get_field_id( 'label' ); ?>"
                name="<?php echo $this->get_field_name( 'label' ); ?>"
                value="<?php echo esc_attr( $label ); ?>" />
        </p>

        <?php

    }

    /**
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update( $new_instance, $old_instance ) {

        $instance = $old_instance;
        $instance['label'] = strip_tags( $new_instance['label'] );

        return $instance;

    }

// Class Range_Comparison_Widget ends here
}