<?php
/**
 * Shortcode callback for [range_comparison_map]
 *
 * @package PinnacleAviation\Plugins\RangeComparison
 */

namespace PinnacleAviation\Plugins\RangeComparison\Display;

/**
 * @param array $atts     Widget attributes.
 *      Optional Args:
 *          string $width    Width of map output - expects 'px' or '%' or other unit.
 *          string $height   Height of map output - expects 'px' or '%' or other unit.
 *
 * @return string         Shortcode Output.
 */
function range_comparison_map_shortcode( $atts ) {

	/**
	 * Required Scripts and Styles
	 */
	add_action( 'wp_enqueue_scripts', function() {
		wp_enqueue_script( 'range-comparison-scripts' );
		wp_enqueue_style( 'range-comparison-styles' );
		wp_enqueue_script( 'google-maps-jsapi' );
	});

    $atts = shortcode_atts(
        Common::range_comparison_map_default_atts(),
        $atts
    );

    return Common::range_comparison_map_markup( $atts );

}