<?php
/**
 * Shortcode callback for [range_comparison_map]
 *
 * @package PinnacleAviation\Plugins\RangeComparison
 */

namespace PinnacleAviation\Plugins\RangeComparison\Display;

/**
 * @param array $atts   Shortcode Attributes.
 *      Optional Args:
 *          string $width    Width of map output - expects 'px' or '%' or other unit.
 *          string $height   Height of map output - expects 'px' or '%' or other unit.
 *
 * @return string       Shortcode Output.
 */
function range_comparison_options_shortcode( $atts ) {

    $atts = shortcode_atts(
        Common::map_options_default_attributes(),
        $atts
    );

    return Common::map_options_markup( $atts );

}