<?php
/**
 * Shortcode for outputting intelligent image-representation of Charter Fleet baggage capacity.
 */

namespace PinnacleAviation\Plugins\RangeComparison\Integrations\Charter_Fleet;

/**
 * Output the display value for Charter Baggage Display
 * @return string
 */
function charter_baggage_display_output() {

	// Get our current post.
	global $post;

	// Ensure we're dealing with charter fleet aircraft here.
	if( 'charter-fleets' !== $post->post_type ) {
		return 'Not supported when not viewing a "Charter Fleet Aircraft" Post Type';
	}

	// Semantics.
	$charter_aircraft_id = $post->ID;

	// Get Internal Baggage Capacity.
	$internal_data = get_post_meta( $charter_aircraft_id, "baggage-capacity-internal", true );
	$internal_val  = preg_replace( "/[^0-9]/", "", $internal_data ); // Ensure we only get numbers.

	// Get External Baggage Capacity.
	$external_data = get_post_meta( $charter_aircraft_id, "baggage-capacity-external", true );;
	$external_val  = preg_replace( "/[^0-9]/", "", $internal_data ); // Ensure we only get numbers.

	// Totals.
	$baggage_total = ( $internal_val + $external_val ) / 2; // We divide by 2 to scale things down.

	// Calculate how to distribute things. We want to Represent 50% with large bags, 30% with small bags, and 20% with
	// golf bags.
	$small_available = 0.3 * $baggage_total;
	$small_bags = $small_available / 8;

	$large_available = 0.5 * $baggage_total;
	$large_bags      = $large_available / 8;

	$golf_available = 0.2 * $baggage_total;
	$golf_bags      = $golf_available / 12;

	// Build our output. For calculated values, let's build a string of <img /> tag markup.
	$return_output = '<div class="baggage">';

	for( $i=0; $i<$small_bags; $i++) {
		$return_output .= '<img src="' . RANGE_COMPARISON_IMAGES . 'charter-luggage-small.jpg" />';
	}
	for( $i=0; $i<$large_bags; $i++) {
		$return_output .= '<img src="' . RANGE_COMPARISON_IMAGES . 'charter-luggage-large.jpg" />';
	}
	for( $i=0; $i<$golf_bags; $i++) {
		$return_output .= '<img src="' . RANGE_COMPARISON_IMAGES . 'charter-luggage-golf.jpg" />';
	}

	$return_output .= '</div>'; // Close .baggage-visual.

	return $return_output;

}