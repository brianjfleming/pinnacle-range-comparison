<?php
/**
 *
 */

namespace PinnacleAviation\Plugins\RangeComparison\Integrations\Charter_Fleet;

use PinnacleAviation\Plugins\RangeComparison\DB\DB_Methods;

/**
 * Register the custom metabox for our Charter Fleet Integration.
 */
function add_charter_fleet_metabox() {

	add_meta_box(
		'charter-fleet-integration',
		__( 'Link to Aircraft' ),
		__NAMESPACE__ . '\\charter_fleet_metabox_render',
		'charter-fleets',
		'normal',
		'default'
	);

}

/**
 * Render the metabox.
 * @param object $post The WordPress post Object.
 */
function charter_fleet_metabox_render( $post ) {

	require_once RANGE_COMPARISON_DIR . '/db/class.db-methods.php';

	$jets_value = get_post_meta( $post->ID, 'aircraft_link_jet', true );

	echo '<select name="aircraft_link_jet" id="aircraft_link_jet" class="widefat" style="display: inline-block; width: 99%">';
	echo '<option value=""' . selected( $jets_value, '', false ) . '>No Jet Link</option>';

	$jets = DB_Methods::get_jets( 'comparison' );

	foreach( $jets as $index => $aircraft ) {
		echo "<option value='{$aircraft->ID}' " . selected( $jets_value, $aircraft->ID, false ) . ">{$aircraft->AcMgfName} {$aircraft->AcName}</option>";
	}

	echo '</select>';

	echo "<hr>";

	$props_value = get_post_meta( $post->ID, 'aircraft_link_prop', true );

	echo '<select name="aircraft_link_prop" id="aircraft_link_prop" class="widefat" style="display: inline-block; width: 99%">';
	echo '<option value=""' . selected( $props_value, '', false ) . '>No Prop Link</option>';

	$props = DB_Methods::get_props( 'comparison' );

	foreach( $props as $index => $aircraft ) {
		echo "<option value='{$aircraft->ID}' " . selected( $props_value, $aircraft->ID, false ) . ">{$aircraft->AcMgfName} {$aircraft->AcName}</option>";
	}

	echo '</select>';

}

/**
 * Save our Data
 *
 * @param int $post_id The WordPress post ID.
 * @return string
 */
function charter_fleet_metabox_save( $post_id ) {

	if( 'charter-fleets' == $_POST['post_type'] ) {
		update_post_meta( $post_id, 'aircraft_link_jet', $_POST['aircraft_link_jet'] );
		update_post_meta( $post_id, 'aircraft_link_prop', $_POST['aircraft_link_prop'] );
	}

}
