<?php
/**
 * Shortcode for outputting intelligent image-representation of Charter Fleet passenger capacity.
 */

namespace PinnacleAviation\Plugins\RangeComparison\Integrations\Charter_Fleet;

/**
 * Output the display value for Charter Passenger Display
 * @return string
 */
function charter_passenger_display_output() {

	global $post;

	// Ensure we're dealing with charter fleet aircraft here.
	if( 'charter-fleets' !== $post->post_type ) {
		return 'Not supported when not viewing a "Charter Fleet Aircraft" Post Type';
	}

	// Semantics.
	$charter_aircraft_id = $post->ID;

	// Get Passenger Capacity. NOTE: this is almost always an intenger, but can be equal to "6 or less"
	$passenger_data = get_post_meta( $charter_aircraft_id, "baggage-capacity-internal", true );
	$passenger_val  = preg_replace( "/[^0-9]/", "", $passenger_data ); // Ensure we only get numbers.

	// Build our output. For calculated values, let's build a string of <img /> tag markup.
	$return_output = '<div class="passenger-visual">';

	for( $i=0; $i<$passenger_val; $i++) {
		$return_output .= '<img src="' . RANGE_COMPARISON_IMAGES . 'charter-passenger.jpg" />';
	}

	$return_output .= '</div>'; // Close .passenger-visual.

	return $return_output;

}