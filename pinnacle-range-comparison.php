<?php
/**
 * Pinnacle Aviation Range Comparison
 *
 * @package     pinnacleaviation/plugins
 * @author      Brian Fleming
 * @copyright   2021 Brian Fleming
 * @license     GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name: Pinnacle Aviation Aircraft Range Comparison
 * Plugin URI:  https://bitbucket.org/brianjfleming/pinnacle-range-comparison/src/master/
 * Description: WordPress plugin built for pinnacleaviation.com to utilize aircraft database and output a JavaScript
 *              and Google Maps API application to compare ranges of all aircraft in the database.
 *
 * Version:     1.2.2
 * Author:      Brian Fleming
 * License:     GPL v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

namespace PinnacleAviation\Plugins\RangeComparison;

use PinnacleAviation\Plugins\RangeComparison\CPT\Instructions;
use PinnacleAviation\Plugins\RangeComparison\CPT\Labor_Rates;
use PinnacleAviation\Plugins\RangeComparison\CPT\Map_Cities;
use PinnacleAviation\Plugins\RangeComparison\CPT\Rate_Sheet;
use PinnacleAviation\Plugins\RangeComparison\DB\DB_Init;

/**
 * Some Useful Constants
 */
define( 'RANGE_COMPARISON_DIST', plugin_dir_url( __FILE__ ) . 'dist/' );   // Quick URL Access to our DIST folder.
define( 'RANGE_COMPARISON_IMAGES', plugin_dir_url( __FILE__ ) . 'images/' );   // Quick URL Access to our DIST folder.
define( 'GOOGLE_MAPS_API_KEY', 'AIzaSyB-hJGb-jaLrSWRf-YXyCixi14iHF7-i1c' );     // Google Maps API Key.
define( 'RANGE_COMPARISON_VERSION', '1.2.3' );                                  // Use this for cache busting.
define( 'RANGE_COMPARISON_DB_VERSION', '1' );                                   // Database Version.
define( 'DB_VERSION_OPTION_KEY', 'prc_db_version' );                            // Key name where we save DB Version.
define( 'MAP_CITIES_POST_TYPE', 'prc_map_cities' );                             // Post type slug for Map Cities.
define( 'RANGE_COMPARISON_CAPABILITY', 'manage_options' );                      // Capability for Map Cities CPT.
define( 'RANGE_COMPARISON_DIR', dirname( __FILE__ ) );                    // Base Directory.


// Include dataBase assets and run install/update check.
add_action( 'init', __NAMESPACE__ . '\\plugin_database_check' );

// Register Google Scripts on Frontend and Admin
add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\plugin_register_scripts_styles' );
add_action( 'admin_enqueue_scripts', __NAMESPACE__ . '\\plugin_register_scripts_styles' );

// Hooks for widget and shortcode registration.
add_action( 'init', __NAMESPACE__ . '\\plugin_register_shortcodes' );
add_action( 'widgets_init', __NAMESPACE__ . '\\plugin_register_widgets' );

// Custom Post Type Registration.
add_action( 'init', __NAMESPACE__ . '\\plugin_register_post_types' );
add_action( 'admin_menu', __NAMESPACE__ . '\\plugin_register_instructions_page' );
add_action( 'admin_menu', __NAMESPACE__ . '\\plugin_register_labor_rates_page' );
add_action( 'admin_menu', __NAMESPACE__ . '\\plugin_register_rate_sheet_page' );

// Integrations
add_action( 'init', __NAMESPACE__ . '\\plugin_load_integrations' );

// AJAX Callbacks
require_once dirname( __FILE__ ) . '/display/ajax.php';
add_action( 'wp_ajax_get_dropdown_options', __NAMESPACE__ . '\\Display\\Ajax\\get_dropdown_options' );
add_action( 'wp_ajax_nopriv_get_dropdown_options', __NAMESPACE__ . '\\Display\\Ajax\\get_dropdown_options' );

add_action( 'wp_ajax_get_plane_details', __NAMESPACE__ . '\\Display\\Ajax\\get_plane_details' );
add_action( 'wp_ajax_nopriv_get_plane_details', __NAMESPACE__ . '\\Display\\Ajax\\get_plane_details' );

/**
 * Register all required Scripts and Styles to support the plugin.
 */
function plugin_register_scripts_styles() {

    // Google Maps API.
    wp_register_script(
        'google-maps-jsapi',
        'https://maps.googleapis.com/maps/api/js?key=' . GOOGLE_MAPS_API_KEY,
        array(),
        RANGE_COMPARISON_VERSION,
        true
    );

    // Range Comparison Scripts.
    wp_register_script(
        'range-comparison-scripts',
        RANGE_COMPARISON_DIST . 'js/range_comparison_scripts.min.js',
        array( 'jquery', 'google-maps-jsapi' ),
        RANGE_COMPARISON_VERSION,
        true
    );

    // FontAwesome 5
	wp_register_script(
		'fontawesome5',
		'https://kit.fontawesome.com/b5e0e5a47e.js',
		array(),
		false
	);

    // Range Comparison Styles.
    wp_register_style(
        'range-comparison-styles',
        RANGE_COMPARISON_DIST . 'css/range_comparison_styles.min.css',
        array(),
        RANGE_COMPARISON_VERSION
    );

    // Aircraft Comparison Scripts
	wp_register_script(
		'aircraft-comparison-scripts',
		RANGE_COMPARISON_DIST . 'js/aircraft_comparison_scripts.min.js',
		array( 'jquery', 'fontawesome5' ),
		RANGE_COMPARISON_VERSION,
		true
	);

	// Localize Labor Rates.
	require_once dirname( __FILE__ ) . '/cpt/class.labor-rates.php';
	$labor_rates = Labor_Rates::get_labor_rates_values();
	wp_localize_script( 'aircraft-comparison-scripts', 'labor_rates', $labor_rates );

	// Aircraft Comparison Styles.
	wp_register_style(
		'aircraft-comparison-styles',
		RANGE_COMPARISON_DIST . 'css/aircraft_comparison_styles.min.css',
		array(),
		RANGE_COMPARISON_VERSION
	);
}

/**
 * Runs install/update check on our database version.
 */
function plugin_database_check() {
    require_once dirname( __FILE__ ) . '/db/class.db-init.php';
    DB_Init::install_update_check();
}

/**
 * Register our Widget(s) for availability in WP Admin.
 */
function plugin_register_widgets() {
    require_once dirname( __FILE__ ) . '/display/class.common.php';
    require_once dirname( __FILE__ ) . '/display/class.range-comparison-map-widget.php';
    require_once dirname( __FILE__ ) . '/display/class.range-comparison-options-widget.php';
    require_once dirname( __FILE__ ) . '/display/class.aircraft-comparison-widget.php';

    register_widget( __NAMESPACE__ . '\\Display\\Range_Comparison_Map' );
    register_widget( __NAMESPACE__ . '\\Display\\Range_Comparison_Options' );
    register_widget( __NAMESPACE__ . '\\Display\\Aircraft_Comparison' );
}

/**
 * Register our Shortcode(s) for availability in WP Admin.
 */
function plugin_register_shortcodes() {
    require_once dirname( __FILE__ ) . '/display/range-comparison-map-shortcode.php';
    require_once dirname( __FILE__ ) . '/display/range-comparison-options-shortcode.php';
    require_once dirname( __FILE__ ) . '/display/aircraft-comparison-shortcode.php';
    require_once dirname( __FILE__ ) . '/integrations/charter-baggage-display-shortcode.php';
    require_once dirname( __FILE__ ) . '/integrations/charter-passenger-display-shortcode.php';

    add_shortcode( 'range_comparison_map', __NAMESPACE__ . '\\Display\\range_comparison_map_shortcode' );
    add_shortcode( 'range_comparison_options', __NAMESPACE__ . '\\Display\\range_comparison_options_shortcode' );
    add_shortcode( 'aircraft_comparison', __NAMESPACE__ . '\\Display\\aircraft_comparison_shortcode' );
    add_shortcode( 'charter_baggage_display', __NAMESPACE__ . '\\Integrations\\Charter_Fleet\\charter_baggage_display_output' );
    add_shortcode( 'charter_passenger_display', __NAMESPACE__ . '\\Integrations\\Charter_Fleet\\charter_passenger_display_output' );
}

/**
 * Register our Custom Post Type(s)
 */
function plugin_register_post_types() {
    require_once dirname( __FILE__ ) . '/cpt/class.map-cities.php';
    Map_Cities::register_post_type();
}

/**
 * Register our How to Use page.
 */
function plugin_register_instructions_page() {
    require_once dirname( __FILE__ ) . '/cpt/class.instructions.php';
    Instructions::register_instructions_page();
}

/**
 * Register our Labor Rates page.
 */
function plugin_register_labor_rates_page() {
	require_once dirname( __FILE__ ) . '/cpt/class.labor-rates.php';
	Labor_Rates::register_labor_rates_page();
}

/**
 * Register our Labor Rates page.
 */
function plugin_register_rate_sheet_page() {
	require_once dirname( __FILE__ ) . '/cpt/class.rate-sheet.php';
	Rate_Sheet::register_rate_sheet_page();
}

/**
 * Adds custom metabox to the 'charter-fleets' edit pages for Integrations with Aircraft Databases.
 */
function plugin_load_integrations() {
	require_once dirname( __FILE__ ) . '/integrations/charter-fleet.php';
	add_action( 'add_meta_boxes', __NAMESPACE__ . '\\Integrations\\Charter_Fleet\\add_charter_fleet_metabox' );
	add_action( 'pre_post_update', __NAMESPACE__ . '\\Integrations\\Charter_Fleet\\charter_fleet_metabox_save' );
}