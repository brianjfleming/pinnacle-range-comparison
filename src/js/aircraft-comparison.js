let version = '1.2.3';
let $ = jQuery.noConflict();



$( '.aircraft_category_select' ).change( async function() {

    const triggeredElementValue = $(this).val();
    const targetElement    = $(this).siblings('.aircraft_select');
    const compID = $(this).attr('data-comp-id');

    let options = await get_dropdown_options( triggeredElementValue );

    $('.comparison_subtable.comp-' + compID ).hide();

    if( 'jets' === triggeredElementValue ) {
        $('.' + compID + '-thrust-reverser').html( 'Thrust Reverser' );
    } else {
        $('.' + compID + '-thrust-reverser').html( 'Propeller' );
    }

    targetElement.find('option').remove().end();

    targetElement.append(new Option("Select an Aircraft", "" ) );

    $.each( options, function( index, data ) {
        const displayName = data.AcMgfName + ' ' + data.AcName;
        const o = new Option( displayName, data.ID );
        $(o).html( displayName );
        targetElement.append(o);
    } );

    targetElement.prop( 'disabled', false ).show();

} );

$( '.aircraft_select').change( async function() {

    var plane_id = $(this).val();
    var comp_id = $(this).attr('data-comp-id');

    var category = $(this).siblings('.aircraft_category_select').val();
    var plane_details = await get_plane_details( plane_id, category );

    $.each( plane_details, function( key, value ) {

        if( 'FuelCostGal' === key ) {
            value = parseFloat( labor_rates.lr_fuel_cost ) * parseFloat( plane_details.FuelFlow );
        }

        if( 'MaintLabor' === key ) {
            if( 'jets' === category ) {
                var labor = labor_rates.lr_jets_cost;
            } else {
                var labor = labor_rates.lr_prop_cost;
            }

            value = parseFloat( labor ) * parseFloat( plane_details.MaintLabor );
        }

        var element = $('*[data-' + category + '="' + comp_id + '-' + key + '"]');

        if( element.hasClass('format_dollars') && '' !== value ) {

            // Strip all non-numeric if we are dealing with a string.
            if( typeof value === 'string' ) {
                value = value.replace(/[^\d\.]+/g, '');
            }

            // Init NumberFormatter.
            var formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD',
                minimumFractionDigits: 2
            });

            // Convert output to USD.
            value = formatter.format(value);
        }

        if( element.attr('data-suffix') && '' !== value ) {
            value = value + ' ' + element.attr('data-suffix');
        }

        element.html(value);
    } );

    $('.' + comp_id + '-labor-rate').hide();
    $('.' + comp_id + '-' + category + '.' + comp_id + '-labor-rate').show();

    $('.comparison_subtable.comp-' + comp_id ).show();
    $('.split').show();

} );

const get_dropdown_options = async ( category ) => {

    return new Promise((resolve,reject) => {
        $.ajax({
            type: "POST",
            url: data.ajaxurl,
            data: {
                action: 'get_dropdown_options',
                category: category
            },
            success: function ( response ) {
                resolve( response );
            },
            error: function (request, status, error) {
                resolve( error );
            }
        });
    } );

};

const get_plane_details = async ( plane_id, category ) => {

    return new Promise( (resolve, reject ) => {

        $.ajax({
            type: "POST",
            url: data.ajaxurl,
            data: {
                action: 'get_plane_details',
                plane_id: plane_id,
                category: category
            },
            success: function( response ) {
                resolve( response );
            },
            error: function( request, status, error ) {
                resolve( error );
            }
        })
    } );
};