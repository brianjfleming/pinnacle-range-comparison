var $ = jQuery.noConflict();

var map_init = false;
var status = {};

// Setup
// ************************************************************************* //
var map;										// map object
var mapOptions;									// map options
var styles;										// map styles

var active_continent = '';						// active continent
var active_city = '';							// active city
var active_plane_a = null;						// active plane a
var active_plane_b = null;						// active plane b
var active_plane_c = null;						// active plane c

var circle_a = null;							// circle object
var circle_b = null;							// circle object
var circle_c = null;							// cirlce object

var range_a = null;								// range value
var range_b = null;								// range value
var range_c = null;								// range value
var maxRange = null;							// maximum active range

var draw_circle = null;
var plane      = null;
var center     = null;
var city       = null;
var activeCity = null;                          // active city

var planeArray = [];							// array of planes
var cityArray = [];								// array of cities

var activeInfoWindow = null;

var windowResize = null;                        // hold timeout for window resize.

var buildingActive = false;

var map_height_original = null;
var map_width_original = null;

var canvas_height_original = null;
var canvas_width_original = null;

$(window).on('load', function() {

    console.log( 'Running with Promises' );

    // Make sure we only fire one time.
    if( false === map_init ) {
        runRangeMap();
    }
} );

async function runRangeMap() {

    if( ! $( '#range_finder' ).length ) {
        return;
    }

    if( true === buildingActive ) {
        return;
    } else {
        buildingActive = true;
    }

    console.log( 'Initializing Range Comparison Map' );

    await setOptions();								// load our map options
    await initMap();                                // run the map
    await initDropdowns();							// initialize the dropdowns
    await buildCityArray();							// city array
    await buildPlaneArray();						// build our plane array

    // Showing active switch between jets and props.
    $('a.tab-select').on( 'click', function() {

        var category = $(this).attr('data-category');
        var tab = $(this).attr('data-tab');

        $('.tab_'+tab).removeClass('active');
        $('#'+category+'_'+tab+'_trigger').addClass('active');

        $('.cat_'+tab).fadeOut(250);
        $('#'+category+'_'+tab).fadeIn(250);

    } );

    $('.range_dropdown').css('top', $('div#range_nav').outerHeight() );

    setEventListeners();
    map_init = true;
    buildingActive = false;
    status.loaded = true;

    $('#map-loading').hide();
    console.log( 'Map Loaded' );

}

async function reinitialize() {

    await setOptions();								// load our map options
    await initMap();
    hardReset();

}

// Map Options														  *complete
// ************************************************************************* //
async function setOptions() {

    return new Promise ( (resolve, reject ) => {

        // create our map options array
        mapOptions = {
            zoom: 4,
            panControl: false,
            scrollwheel: false,
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL,
                position: google.maps.ControlPosition.LEFT_BOTTOM
            },
            scaleControl: true,
            streetViewControl: false,
            mapTypeControl: false,
            center: new google.maps.LatLng(39.707186,-101.60156),
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
            },
            fullscreenControl: false
        };

        styles = [
            {
                "featureType": "water",
                "stylers": [
                    { "color": "#808080" }
                ]
            },{
                "featureType": "road.highway",
                "stylers": [
                    { "visibility": "off" }
                ]
            },{
                "featureType": "road",
                "stylers": [
                    { "visibility": "off" }
                ]
            },{
                "featureType": "landscape.man_made",
                "stylers": [
                    { "visibility": "off" }
                ]
            },{
                "featureType": "road",
                "stylers": [
                    { "visibility": "on" },
                    { "saturation": -100 },
                    { "gamma": 2.15 }
                ]
            },{
                "featureType": "landscape.natural",
                "stylers": [
                    { "visibility": "on" },
                    { "hue": "#00a1ff" },
                    { "saturation": -100 }
                ]
            },{
                "featureType": "administrative.country",
                "stylers": [
                    { "visibility": "on" },
                    { "color": "#808080" },
                    { "weight": 0.5 },
                    { "gamma": 1 },
                    { "lightness": 16 }
                ]
            },{
                "featureType": "administrative.province",
                "stylers": [
                    { "visibility": "on" },
                    { "weight": 1 }
                ]
            },{
                "featureType": "administrative.province",
                "elementType": "labels",
                "stylers": [
                    { "visibility": "off" }
                ]
            },{
                "featureType": "administrative.locality",
                "stylers": [
                    { "visibility": "off" }
                ]
            },{
                "featureType": "administrative.neighborhood",
                "stylers": [
                    { "visibility": "off" }
                ]
            },{
                "featureType": "landscape.natural.landcover",
                "stylers": [
                    { "visibility": "on" }
                ]
            },{
            }
        ];

        resolve();

    } );

}



// Map Object														  *complete
// ************************************************************************* //
async function initMap() {

    return new Promise ( (resolve, reject ) => {

        var styledMap = new google.maps.StyledMapType(styles, {name: "Pinnacle"});
        map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');

        resolve();

    } );


}


// Removes all cicles and labels - hard reset						  *complete
// ************************************************************************* //
async function hardReset() {

    if(circle_a != null) {circle_a.setMap(null); circle_a = null; }
    if(circle_b != null) {circle_b.setMap(null); circle_b = null; }
    if(circle_c != null) {circle_c.setMap(null); circle_c = null; }
    if(activeInfoWindow != null) { activeInfoWindow.close(); }

    $('#continent_selection').html('None');
    $('#city_selection').html('None');
    $('#plane_a_selection').html('None');
    $('#plane_b_selection').html('None');
    $('#plane_c_selection').html('None');

    active_continent = '';
    active_city = '';
    active_plane_a = null;
    active_plane_b = null;
    active_plane_c = null;

    range_a = null;
    range_b = null;
    range_c = null;
    maxRange = null;

    await buildCityArray();

}


// Dropdowns														  *complete
// ************************************************************************* //
async function initDropdowns() {

    return new Promise ( (resolve, reject ) => {

        // Step 1: columnize our lists
        $("#city_dd").tinyscrollbar();
        $("#plane_a_dd").tinyscrollbar();
        $("#plane_b_dd").tinyscrollbar();
        $("#plane_c_dd").tinyscrollbar();

        // Step 2: hide all the dropdowns and city groups
        // then turn on our default city group and prompt
        $('.range_dropdown, .city_group').hide();
        $('#default_cities, #range_prompt').show();

        // Step 3: set up our event listeners
        // these will make the dropdowns work

        // Opens Dropdowns - closes any other
        $('.dropdown_trigger').click(function(event) {
            event.stopPropagation();
            var targetEle = $(this).attr('rel');
            $('#range_prompt').fadeOut(200);
            $('.range_dropdown:not(#'+targetEle+')').hide(200);
            $('#'+targetEle).show(200);
        });

        // Keeps dd-clicks from closing itself
        $('.range_dropdown').click(function(event) {
            event.stopPropagation();
        });

        // Closes dropdowns on click-off
        $(document).click(function() {
            $('.range_dropdown').hide(200);
        });

        // Runs our selectors for our dropdowns
        initContinentSelect();
        initCitySelect();
        initPlaneSelect();

        resolve();
    } );

}


// Event listener for selecting a continent 						  *complete
// ************************************************************************* //
function initContinentSelect() {
    $('#continent_dd > a').click(function() {
        $('#continent_selection').html($(this).html())
        $('#continent_dd').hide(200);

        active_continent = $(this).attr('rel');

        //re-center our map
        switch(active_continent) {
            case 'africa':
                var center = new google.maps.LatLng(7.7109916,15.8203125);
                var zoomLevel = 3;
                break;

            case 'asia':
                var center = new google.maps.LatLng(54.876606,86.484375);
                var zoomLevel = 3;
                break;

            case 'australia':
                var center = new google.maps.LatLng(-25.08559,134.384765);
                var zoomLevel = 4;
                break;

            case 'europe':
                var center = new google.maps.LatLng(49.03786,12.216796);
                var zoomLevel = 4;
                break;

            case 'north_america':
                var center = new google.maps.LatLng(39.707186,-101.60156);
                var zoomLevel = 4;
                break;

            case 'south_america':
                var center = new google.maps.LatLng(-15.114552,-58.53515);
                var zoomLevel = 4;
                break;

        }

        map.panTo(center);

        $('.city_group').hide();
        $('#city_'+active_continent).show();
        $('#city_dd').show(200);
        $("#city_dd").tinyscrollbar_update();
    });
}


// Event listener for selecting a city								  *complete
// ************************************************************************* //
function initCitySelect() {
    $('#city_dd > div > div > div > a').click(function() {
        var id = $(this).attr('data-id');
        setFocus(id);
        $('#city_dd').hide(200);

        // save the active city
        activeCity = id;
    });
}


// Event listener for selecting planes								  *complete
// ************************************************************************* //
function initPlaneSelect() {
    var id;
    // plane a
    $('#plane_a_dd > div > div > div > a').click(function() {
        id = $(this).attr('data-id');
        $('#plane_a_dd').hide(200);

        setPlane('a',id);
    });

    // plane b
    $('#plane_b_dd > div > div > div > a').click(function() {
        id = $(this).attr('data-id');
        $('#plane_b_dd').hide(200);

        setPlane('b',id);
    });

    // plane c
    $('#plane_c_dd > div > div > div > a').click(function() {
        id = $(this).attr('data-id');
        $('#plane_c_dd').hide(200);

        setPlane('c',id);
    });
}


// Center the map (arg1 = google lat,lng object)					  *complete
// ************************************************************************* //
function centerMap(x) {
    map.panTo(x);
}


// Draws a marker on the map										  *complete
// ************************************************************************* //
function addMarker(lat,lng,icon,id) {

    var myLatLng = new google.maps.LatLng(lat,lng);
    var marker = new google.maps.Marker({
        map: map,
        draggable: false,
        position: myLatLng,
        visible: true
    });

    var image = new google.maps.MarkerImage(
        icon,
        new google.maps.Size(16,16)
    );

    marker.setIcon(image);

    google.maps.event.addListener(marker, 'click', function() {
        setFocus(id);
    });
    google.maps.event.addListener(marker, 'mouseover', function() {
        showName(id);
    });
    google.maps.event.addListener(marker, 'mouseout', function() {
        hideName(id);
    });

    return marker;
}


// showName
// ************************************************************************* //
function showName(id) {
    var city = getCity(id);
    if(activeCity != id) {
        city.infowindow.open(map,city.marker);
        stop();
    } else {
        stop();
    }
}


// Adds an InfoBubble to marker										  *complete
// ************************************************************************* //
function hideName(id) {
    city = getCity(id);
    if(activeCity != id) {
        city.infowindow.close();
    }
}


// Adds an InfoBubble to marker										  *complete
// ************************************************************************* //
function addInfoBubble(content) {

    var infoBubble = new InfoBubble({
        map: map,
        content: content,
        hideCloseButton: true,
        backgroundColor: '#595959',
        borderRadius: 0,
        borderWidth: 0,
        minWidth: 160,
        maxWidth: 500,
    });


    return infoBubble;
}

// Builds a useable array of cities									  *complete
// ************************************************************************* //
async function buildCityArray() {

    return new Promise ( (resolve, reject ) => {

        $("#city_dd > div > div > div").children().map(function() {
            if($(this).attr('data-primary') == 'true') {
                var icon = '/wp-content/plugins/pinnacle-range-comparison/dist/images/range_hq.png';
            } else {
                var icon = '/wp-content/plugins/pinnacle-range-comparison/dist/images/range_dot.png';
            }
            var lat = $(this).attr('data-lat');
            var lng = $(this).attr('data-lng');
            var name = $(this).html().replace(/ /g, '\u00a0');
            var id = $(this).attr('data-id');
            var content = '<span class="info-title">' + name + '</span>';
            var latLngObj = new google.maps.LatLng(lat,lng);


            // draws marker
            if(lat != '' && lng != '') {
                var marker = addMarker(lat,lng,icon,id);
                var infowindow = addInfoBubble(content);

                var x = [id, name, marker, infowindow, latLngObj];
                cityArray.push(x);
            }
        });

        resolve();
    } );
}

// Gets all our city information									  *complete
// ************************************************************************* //
function getCity(id) {
    var city = new Object;
    for( var i = 0, len = cityArray.length; i < len; i++) {
        if(cityArray[i][0] == id) {
            city.id 		= cityArray[i][0];
            city.name 		= cityArray[i][1];
            city.marker 	= cityArray[i][2];
            city.infowindow = cityArray[i][3];
            city.latlng 	= cityArray[i][4];

            break;
        }
    }

    return city;
}

// Gets all our palne information									  *complete
// ************************************************************************* //
function getPlane(id) {
    var plane = new Object;
    for( var i = 0, len = planeArray.length; i < len; i++) {
        if(planeArray[i][0] == id) {
            plane.id 		= planeArray[i][0];
            plane.name 		= planeArray[i][1];
            plane.r1 		= planeArray[i][2];
            plane.r2 		= planeArray[i][3];
            plane.r3 		= planeArray[i][4];
            break;
        }
    }

    return plane;
}

// Builds an array of planes										  *complete
// ************************************************************************* //
async function buildPlaneArray() {

    return new Promise ( (resolve, reject ) => {

        if( 0 < planeArray.length ) {
            resolve();
        }

        $("#plane_a_dd > div > div > div").children().map(function() {
            var name = $(this).html();
            var id = $(this).attr("data-id");
            var range_a = $(this).attr("data-r1");
            var range_b = $(this).attr("data-r2");
            var range_c = $(this).attr("data-r3");

            // build index array
            var i = [id, name, range_a, range_b, range_c];

            planeArray.push(i);
        });
        $("#plane_b_dd > div > div > div").children().map(function() {
            var name = $(this).html();
            var id = $(this).attr("data-id");
            var range_a = $(this).attr("data-r1");
            var range_b = $(this).attr("data-r2");
            var range_c = $(this).attr("data-r3");

            // build index array
            var i = [id, name, range_a, range_b, range_c];

            planeArray.push(i);
        });
        $("#plane_c_dd > div > div > div").children().map(function() {
            var name = $(this).html();
            var id = $(this).attr("data-id");
            var range_a = $(this).attr("data-r1");
            var range_b = $(this).attr("data-r2");
            var range_c = $(this).attr("data-r3");

            // build index array
            var i = [id, name, range_a, range_b, range_c];

            planeArray.push(i);
        });

        resolve();
    } );
}

// Big Daddy - sets the focus on the current city					  *complete
// ************************************************************************* //
function setFocus(id) {
    //find the requested city array and aggregate info
    city = getCity(id);
    activeCity = id;

    // do stuff with a result
    if(city.id) {
        // center the map
        centerMap(city.latlng);

        // show the info window
        if(activeInfoWindow != null) { activeInfoWindow.close(); }
        activeInfoWindow = city.infowindow;
        city.infowindow.open(map,city.marker);

        // update the dropdown selection status
        $("#city_selection").html(city.name);

        // move active rings
        if(circle_a != null) { circle_a.setCenter(city.latlng); }
        if(circle_b != null) { circle_b.setCenter(city.latlng); }
        if(circle_c != null) { circle_c.setCenter(city.latlng); }

    } else {
        alert('Record not found for id ' + id);
    }
}

// Reset Range
// ************************************************************************* //
function setRange() {
    if(active_plane_a != null) { setPlane('a', active_plane_a); }
    if(active_plane_b != null) { setPlane('b', active_plane_b); }
    if(active_plane_c != null) { setPlane('c', active_plane_c); }
}

// Big Daddy - draws the plane rings and such						  *complete
// ************************************************************************* //
function setPlane(which, id) {
    // get our active city;
    city = getCity(activeCity);

    center = city.latlng;

    // get the plane
    plane = getPlane(id);

    // set our radius depending on what range we want
    var rad = $("#range-option").val();
    var radius;
    switch(rad) {
        case 'r1':
            radius = plane.r1;
            break;

        case 'r2':
            radius = plane.r2;
            break;

        case 'r3':
            radius = plane.r3;
            break;
    }


    // draw and set the appropriate ring
    switch(which) {
        case 'a':
            var color = '#dd002b';
            range_a = radius;
            maxRange = Math.max(range_a,range_b,range_c);
            if(circle_a != null) { circle_a.setMap(null); }
            circle_a = drawCircle( parseInt(radius), center, color );
            $("#plane_a_selection").html(plane.name);
            active_plane_a = plane.id;
            break;

        case 'b':
            var color = '#ffffff';
            range_b = radius;
            maxRange = Math.max(range_a,range_b,range_c);
            if(circle_b != null) { circle_b.setMap(null); }
            circle_b = drawCircle( parseInt(radius), center, color );
            $("#plane_b_selection").html(plane.name);
            active_plane_b = plane.id;
            break;

        case 'c':
            var color = '#565656';
            range_c = radius;
            maxRange = Math.max(range_a,range_b,range_c);
            if(circle_c != null) { circle_c.setMap(null); }
            circle_c = drawCircle(parseInt(radius),center,color);
            $("#plane_c_selection").html(plane.name);
            active_plane_c = plane.id;
            break;
    }
}


// Draws the cirlces												  *complete
// ************************************************************************* //
function drawCircle(rad,center,color) {
    // convert nautical miles to miles
    //rad *= .868976;

    // if in miles, convert to meters
    rad *= 1852;

    draw_circle = new google.maps.Circle({
        center: center,
        radius: rad,
        strokeColor: color,
        strokeOpacity: 1,
        strokeWeight: 1,
        fillColor: "#FFFFFF",
        fillOpacity: 0.2,
        map: map
    });


    if(maxRange <= 1000) {
        map.setZoom(5);
    } else if(maxRange <= 1800) {
        map.setZoom(4);
    } else if(maxRange <= 3000) {
        map.setZoom(3);
    } else if(maxRange >= 3001) {
        map.setZoom(2);
    }

    return draw_circle;
}

function setEventListeners() {

    // setTimeout( function() {
    //
    // $(window).on( 'resize', function() {
    //     if( true === map_init ) {
    //         clearTimeout(windowResize);
    //         windowResize = setTimeout(function () {
    //             console.log( 'resize - rebuild' );
    //             runRangeMap();
    //         }, 300);
    //     }
    // });
    //
    // }, 10000 );

    map_height_original = $('div#range_finder').height();
    map_width_original = $('div#range_finder').width();

    canvas_height_original = $('div#map_canvas').height();
    canvas_width_original = $('div#map_canvas').width();

    $('a.map-fullscreen').click(function() {

        $(this).toggleClass('fullscreen-active');

        if($(this).hasClass('fullscreen-active')) {

            $('body,html').css('overflow-y','hidden').css('height','100%').css('position','relative');
            var header_height = $('header#main-header').outerHeight();
            var range_nav_height = $('div#range_nav').outerHeight();

            if( $('#wpadminbar').length ) {
                header_height += 32;
            }

            $('div#range_finder').prependTo('body');
            $('div#map_options').prependTo('body');

            $('#map-loading').show();

            $(this).html('<i class="fas fa-expand-arrows-alt"></i> &nbsp;&nbsp; Minimize');

            $('div#range_finder').css({
                position: 'fixed',
                top: header_height + 'px',
                right: '0',
                left: '0',
                bottom: '0',
                margin: '0',
                height: 'auto',
                width: 'auto',
            }).css('z-index','99999');

            var canvas_adjust = range_nav_height - 27;
            var map_canvas_height = 'calc(100% - ' + canvas_adjust + 'px)';

            $('div#map_canvas').css({
                position: 'absolute',
                top: range_nav_height + 'px',
                left: '0',
                right: '0',
                bottom: '-27px',
                height: map_canvas_height
            });
            $('div#map_options').css({
                position: 'fixed',
                bottom: '9px',
                left: '60px',
                padding: '0 12px 12px 12px',
            }).css('z-index','999999');
            $('#fullscreen').css({
                bottom: '8px',
                left: '260px'
            });

            reinitialize();
        } else {
            $('body,html').css('overflow-y','auto').css('height','auto');
            $('div#range_finder').insertBefore('div#range_finder_marker' );
            $('div#map_options').insertBefore( 'div#map_options_marker' );

            $('#map-loading').show();

            $(this).html('<i class="fas fa-expand-arrows-alt"></i> &nbsp;&nbsp; Fullscreen');

            // // Minimize it
            $('div#range_finder').css({
                position: 'relative',
                top: '0',
                left: '0',
                height: map_height_original,
                width: map_width_original,
            }).css('z-index','99999');
            $('div#map_canvas').css({
                height: canvas_height_original,
                width: '100%'
            });
            $('div#map_options').css({
                position: 'relative',
                padding: '6px 6px 12px 25px',
                left: 'auto',
                bottom: 'auto',
            });
            $('#fullscreen').removeAttr('style');

            reinitialize();
        }

        $('#map-loading').hide();
    });

    // Switch range configuration.
    $(document).on( 'change', 'select#range-option', function() {
        setRange();
    } );

    // Hard Reset.
    $(document).on( 'click', '#range-hard-reset', function() {
        hardReset();
        $("#range_prompt").fadeIn(200);
    } );

}